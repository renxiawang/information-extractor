#coding=utf-8

import nlpir
import re
from pymongo import MongoClient

def load_data():
    '''Connect to the database and load all sample data items of people.

    args: 
        None

    returns: 
        people: a list of people, each item in people is a dict type, for example:
            [{
                "name":"steve", 
                "desc":"a bullshit"
             }, 
             { 
                "name":"evets", 
                "desc":"keeg a"
             },
             ...
             ]

    raises:
        None
    '''
    # connect to localhost MongoDB
    client = MongoClient()

    # Get the renwu database
    renwu = client.renwu

    # Get the baidu_sample collection
    baidu_sample = renwu.baidu_sample_500

    # Create a list object
    people = []

    # Get data from database and store in the people list
    for person in baidu_sample.find():
        people.append(person)

    return people

def split_sentences(desc):
    '''split a chunk of text into sentences, by character '。'

    args: 
        desc: a description text for one person, for example:
            "男，1955年6月生，山东蓬莱人，北京师范大学博士，
            华南师范大学心理学系教授、博士生导师。
            主要研究领域：认知心理学和语言心理学。"

    returns: 
        sentences: a list of sentences, for example:
            [
                "男",
                "1955年6月生",
                "山东蓬莱人",
                "北京师范大学博士",
                "华南师范大学心理学系教授、博士生导师",
                "主要研究领域：认知心理学和语言心理学",
                ""
            ]

    raises:
        None
    '''
    sentences = []
    regex = ur'，|。'

    # desc = desc.split('\n')

    # for line in desc:
    #     sentences.append(line.strip().split(u"。"))
    results = re.split(regex, desc)
    results.pop() # delete the last empty string
    sentences.extend(results)

    return sentences

def segement_words(sentences):
    '''segement words in sentences with pos tagging

    args: 
        sentences: a set of sentences, for example:
        [
                "男",
                "1955年6月生",
                "山东蓬莱人",
                "北京师范大学博士",
                "华南师范大学心理学系教授、博士生导师",
                "主要研究领域：认知心理学和语言心理学",
                ""
        ]

    returns: 
        segemented_sentences: a set of tagged and segemented sentences, for example:
        [
            "男/nse", 
            "1955年/t 6月/t 生/v",
            "山东/ns 蓬莱/ns 人/n", 
            "北京师范大学/nte 博士/nde", 
            "华南师范大学/nte 心理学/nm 系/v 教授/nti 、/wn 博士生导师/np", 
            "主要/b 研究/vn 领域/n ：/wp 认知/v 心理学/nm 和/cc 语言/n 心理学/nm"
        ]

    raises:
        None
    '''
    # init nlpir
    # TODO: Move to init function
    segemented_sentences = []

    # segement sentences
    for one in sentences:
        tokens = nlpir.process_str(one.encode('gb2312', 'ignore').strip(), 1)
        segemented_sentences.append(tokens.decode('gb2312').encode('utf-8'))
        
    return segemented_sentences

def extract_known_entities(sentences):
    '''remove pos taggs excepts those represents the known entities

    args:
        sentences: a set of tagged and segemented sentences, for example:
        [
            "男/nse", 
            "1955年/t 6月/t 生/v",
            "山东/ns 蓬莱/ns 人/n", 
            "北京师范大学/nte 博士/nde", 
            "华南师范大学/nte 心理学/nm 系/v 教授/nti 、/wn 博士生导师/np", 
            "主要/b 研究/vn 领域/n ：/wp 认知/v 心理学/nm 和/cc 语言/n 心理学/nm"
        ]

    returns:

        extracted_sentences: sentences that the known entities are tagged, for example:
        [
            "男，1955年6月生，山东蓬莱人， 
            北京师范大学/nte 博士/nde ，华南师范大学/nte 心理学/nm 系 教授/nti 
            、/wn 博士生导师/np"
        ] 
    '''

    # Kinds of entity to be extratced:
    # education: net, nm, nde
    # employment: np, ntc, ntg, nti
    # 
    entities_tags = ['nde','ntc','nte','ntr','ntg','nm','np','nra','ns','nti','nh','nse','t'] # Currently focus on Education Experience
    extracted_sentences = []

    for one in sentences:
        if one.find(' ') != -1: # TODO
            tokens = one.split(' ')
            tokens.pop() # delete the last empty string element
        else:
            tokens = []
            tokens.append(one)        
        extracted_tokens = []

        # remove unneeded tags
        for tok in tokens:
            if tok.find('/') != -1:
                if tok.split('/')[1] in entities_tags:
                    extracted_tokens.append(' ' + tok + ' ')
                else:
                    extracted_tokens.append(tok.split('/')[0])
            else:
                continue

        extracted_sentences.append(''.join(extracted_tokens))

    return extracted_sentences

def classify_sentences(extracted_sentences):
    # connect to localhost MongoDB
    client = MongoClient()

    # Get the renwu database
    renwu = client.renwu

    edu_regx = r'nte|nm|nde'
    # empl_regx = r'np|ntc|ntg|nti'

    for one in extracted_sentences:
        match_edu = re.search(edu_regx, one)
        # match_empl = re.search(empl_regx, one)

        if match_edu:
            renwu.corpus.update({"type": "edu"},{"$push": {"sentences": one}})
        
        # if match_empl:
        #    renwu.corpus.update({"type": "empl"},{"$push": {"sentences": one}})

def main():
    nlpir.ict_init("/workspace/packages/NLPIR/nlpirpy_ext")
    # nlpir.import_dict('/workspace/packages/NLPIR/nlpirpy_ext/userdic.txt')

    people = load_data()

    for person in people:
        sentences = split_sentences(person['desc'])
        segemented_sentences = segement_words(sentences)
        extracted_sentences = extract_known_entities(segemented_sentences)
        classify_sentences(extracted_sentences)


if __name__ == '__main__':
    main()
