# -*- coding: utf-8 -*-

import nlpir
    
print nlpir.ict_init("/workspace/packages/NLPIR/nlpirpy_ext") # path to ICTCLAS2012
# t_filename = "sentences.txt" # sample text for segmentation
# tweet_file = open(t_filename, 'r')
# for line in tweet_file.readlines():
#     string2 = line.decode('utf-8').encode('gb2312', 'ignore')
#     tokens = nlpir.process_str(string2, 1)
#     print tokens.decode('gb2312').encode('utf-8')
text = '''
上赛季这名23岁的球员在荷甲联赛给人留下了深刻的印象，在通过努力夺取主力地位后,他取得了21次打满全场的记录。PSV也渴望留下这位上赛季球队的主力后卫，他们已经在设法说服斯坦福桥延长这名球员一年的租借期。但是英国人更愿意将这名球员出售，目前唯一的问题是据说PSV无法负担得起这位巴西后卫的工资。
　　阿尔西德斯现在仍然跟随PSV在进行赛季前的训练，但是他现在仍然无法确定自己是否会留在这里。
　　“切尔西想要卖掉我，所以我也许无法留在埃因霍温。但是我真的很享受在这里的生活并且我希望切尔西能再租借我一个赛季。”
　　阿尔西德斯2006年加盟PSV，在此之前他曾效力过沙尔克04（Schalke 04）、桑托斯（Santos）和本菲卡（Benfica ）。这名希望之星在2004年被切尔西购入，但随后他立即被俱乐部租借至本菲卡。
　　他是参加03年南美U20青年赛巴西青年队的主力中卫，也是最年轻的一位。由于表现出色，被沙尔克04队招入，对先天条件极好的阿尔西德斯来说，18岁就能到德甲强队锻炼，对他成长大有裨益。德国曾经培养过许多世界级的中卫，巴西也有卢西奥这样的名将在此成材，阿尔西德斯的前景可以看好。
'''#.decode('utf-8').encode('gb2312', 'ignore')
# print nlpir.import_dict('/workspace/packages/NLPIR/nlpirpy_ext/userdic.txt')

sentences = []
sentences.extend(text.strip().split("。"))

segemented_sentences = []
for one in sentences:
    tokens = nlpir.process_str(one, 1)
    segemented_sentences.append(tokens)

print segemented_sentences
#tokens = nlpir.process_str(text, 0)
#print tokens.decode('gb2312').encode('utf-8')
print nlpir.ict_exit()
