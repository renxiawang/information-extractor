/*
 * Get documents of people whose name is in the top 50 list
 */

// login to source database
var connection = new Mongo('localhost:27017')
var source_db = connection.getDB('search_people')

source_db.baidu_mr_50.find().forEach(
		function(name) {
			source_db.baidu_extracted.find({"name" : name._id}).forEach(
					function(person) {
						source_db.baidu_same_name.insert(person);
					}
				);
		}
	);

source_db.hudong_mr_50.find().forEach(
		function(name) {
			source_db.hudong_extracted.find({"name" : name._id}).forEach(
					function(person) {
						source_db.hudong_same_name.insert(person);
					}
				);
		}
	);