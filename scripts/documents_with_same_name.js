/*
 * Use map-reduce to count the number of documents with same name
 * Store the result in a new collection
 * Get top 50s in both new collection
 * Store the result to a new collection
 */

// login to source database
var connection = new Mongo('localhost:27017')
var source_db = connection.getDB('search_people')

var map = function(){
   if(this.name) {
        emit(this.name, 1);
   }
}

var reduce = function(key, values){
    var result = 0;
    values.forEach(function(value) {
      result += value;
    });
    return result;
}

//source_db.baidu_extracted.mapReduce(map, reduce, {out: "baidu_mr"});
// source_db.hudong_extracted.mapReduce(map, reduce, {out: "hudong_mr"});

var cursor_baidu = source_db.baidu_mr.find().sort({value: -1}).limit(50);
while(cursor_baidu.hasNext()) {
	source_db.baidu_mr_50.insert(cursor_baidu.next())
}
	
var cursor_hudong = source_db.hudong_mr.find().sort({value: -1}).limit(50);
while(cursor_hudong.hasNext()) {
	source_db.hudong_mr_50.insert(cursor_hudong.next())
}
