#!/usr/bin/python
# -*- coding: utf-8 -*-

import nlpir
import re
from pymongo import MongoClient
import json
from bson import json_util

def segment_words(content):
    tokens = nlpir.process_str(content.encode('gb2312', 'ignore').strip(), 0)
    return tokens.decode('gb2312').encode('utf-8')

def del_stopwords(tokens):
    tokens_without_stops = []

    tokens = re.sub('\d|\w|[\uFF10-\uFF19\uFF21-\uFF3A\uFF41-\uFF5A]+', '', tokens)

    for tok in tokens.split(' '):
        if tok not in stopwords:
            tokens_without_stops.append(tok)

    return tokens_without_stops

def print_dic_values(dic):
    for k, v in dic.iteritems():
        if k not in ['description', 'dataSource', '_id', 'baidu_id', 'hudong_id', 'photo', 'timestamp']:
            if isinstance(v, dict):
                print_dic_values(v)
            elif isinstance(v, list):
                print_list_values(v)
            else:
                keywords_list.append(v)

def print_list_values(lis):
    for v in lis:
        if isinstance(v, dict):
            print_dic_values(v)
        elif isinstance(v, list):
            print_list_values(v)
        else:
            keywords_list.append(v)

# init nlpir    
nlpir.ict_init("/workspace/packages/NLPIR/nlpirpy_ext")

# load stopwords
f = open('/workspace/fyp/chinese_stopwords_list.txt', 'r')
stopwords = []
for one in f.readlines():
    stopwords.append(one.strip())

# connect mongodb
client = MongoClient()
renwu = client.renwu_fyp
combined = renwu.baidu_hudong
text_search_enabled = renwu.people

keywords_list = []

for person in combined.find():
    print_dic_values(person)
    keywords_str = ' '.join(keywords_list)
    keywords_str = re.sub(ur"\d+|\w+|[(|)|,|，|/|=|+|\[|\]|.|。|‘|’|\-|——|!|！|\@|＠|＃|#|￥|\$|？|、|【|】|\"|\"|\'|\'|?|\\|、|\||｜|;|:|：|；|\{|\}|｛|｝|《|》|_|\||＋|<|>|`|~|～|｀|*|×|\"|\"|“|”|（|）| |　|－]", ' ', keywords_str.strip())
    keywords_str = re.sub(ur"\s+", ' ', keywords_str.strip())
    
    person['keywords'] = keywords_str
    text_search_enabled.insert(person)
    keywords_list = []
    # if 'description' in person:
    #     descriptions = person['description']

    #     all_segmented_contents = []
    #     for one_desc in descriptions:
    #         segmented_words = segment_words(one_desc['content'])
    #         words_without_stops = del_stopwords(segmented_words)

    #         all_segmented_contents.extend(words_without_stops)

    #     string = json.dumps(person, default=json_util.default)
    #     #string = re.sub('\d|\w|[\uFF10-\uFF19\uFF21-\uFF3A\uFF41-\uFF5A]+', '', string)
    #     string = re.sub('[^\u4e00-\u9fa5]+', '', string)
    #     print string
    #     # person['segmented_words'] = ' '.join(set(all_segmented_contents))
    #     #text_search_enabled.insert(person)
    #     break
    # else:
    #     #text_search_enabled.insert(person)
    #     pass
        
