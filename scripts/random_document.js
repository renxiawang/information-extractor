/*
 * Select 30 documents randomly from source database
 * Store the result into another database
 */


// login to source database
var connection = new Mongo('localhost:27017')
var source_db = connection.getDB('search_people')

// select 30 document randomly
var count = source_db.baidu.count()
var rand = function() {
		return Math.floor(Math.random() * count)
	}



// store the result to another
var dest_db = connection.getDB('test_only')
var i = 100
while(i--) {
	var cursor = source_db.baidu.find().limit(-1).skip(rand()).next();
	dest_db.test_people.insert(cursor)
}
