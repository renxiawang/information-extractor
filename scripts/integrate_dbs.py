#!/usr/bin/python
# -*- coding: utf-8 -*-

from pymongo import MongoClient

# connect mongodb
client = MongoClient()
renwu = client.renwu_fyp
baidu = renwu.baidu_extracted
hudong = renwu.hudong_extracted
combined = renwu.baidu_hudong

for person in baidu.find():
    if combined.find_one({"baidu_id": person['_id']}):
        pass
    else:
        print 'baidu one new'
        combined.insert(person)

for person in hudong.find():
    if combined.find_one({"hudong_id": person['_id']}):
        pass
    else:
        print 'hudong one new'
        combined.insert(person)