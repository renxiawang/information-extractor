\chapter{Solution Description}\label{cha:solude}
This chapter discusses the details of the information extraction and duplicates elimination problems being solved. The implementations of approaches are illustrated. Section \ref{sec:wst} focus on solving the fundamental problems which are word segmentation and tagging. The name entity recognition problem and its solution are presented in section \ref{sec:ner}. The implementation of solving the entity relation recognition problem is given in section \ref{sec:err}. At the end of this chapter the approach of duplicates elimination is discussed in \ref{sec:dei}.

\section{Word Segmentation and Tagging}\label{sec:wst}
Word segmentation and tagging are the elementary tasks in information extraction. Unlike English, Chinese has no distinct separator between words, which lead to difficulties, including \textit{unknown words} and \textit{segmentation ambiguity}, in word segmentation. The ICTCLAS provides high accuracy and efficiency solution to resolve this problem. To adjust the ICTCLAS to the specific problem being solved, a user dictionary with a customized tag set are involved.

\subsection{User Dictionary}
The purpose of word segmentation and tagging is to prepare words for name entity recognition. Correct segmentation on known name entity is important for the further processing. By this consideration, I collect a dictionary of known name entities. The details statistics of the dictionary is showed in Table \ref{tab:udict}.
\begin{table}[h]
  \centering
  \begin{tabular}{ l | c || r }
  \textbf{Entity Type} & \textbf{Count} & \textbf{Example} \\
  \hline
  Degree & 5 & 学士 \\
  \hline
  Company & 1392 & 中国人民保险公司 \\
  \hline
  Education & 550564 & 清新县石潭镇南楼小学 \\
  \hline
  Labs & 1007 & 中国医学科学院输血研究所 \\
  \hline
  Government & 1631 & 鹰潭市人民政府 \\
  \hline
  Major & 884 & 计算机科学与技术 \\
  \hline
  Position & 1928 & 营销经理 \\
  \hline
  Race & 455 & 密克罗尼西亚族 \\
  \hline
  Address & 3531 & 赤城县 \\
  \hline
  Title & 52 & 考古学家 \\
  \hline
  Honor & 1492 & 雅典奥运会48公斤级金牌 \\
  \hline
  Gender & 2 & 男 \\
  \hline
  Total & 563534 &
  \end{tabular}
  \caption{User Dictionary}\label{tab:udict}
\end{table}
\subsection{Pre Name Entity Tags}
Except the POS tag set defined in the ICTCLAS\cite{ictclas}, I defined a new tag set, \textit{pre name entity tag(PNET)}, for tagging the known name entity during the word segmentation process. The entity types and their corresponding tags are listed in Table \ref{tab:pnet}.
\begin{table}[h]
  \centering
  \begin{tabular}{ l | r }
  \textbf{Entity Type} & \textbf{Tag} \\
  \hline
  Degree & nde \\
  \hline
  Company & ntc \\
  \hline
  Education & nte \\
  \hline
  Labs & ntr \\
  \hline
  Government & ntg \\
  \hline
  Major & nm \\
  \hline
  Position & np \\
  \hline
  Race & nra \\
  \hline
  Address & ns \\
  \hline
  Title & nti \\
  \hline
  Honor & nh \\
  \hline
  Gender & nse \\
  \end{tabular}
  \caption{Pre Name Entity Tags}\label{tab:pnet}
\end{table}
\subsection{Implementation}
The word segmentation and tagging tasks are accomplished by the ICTCLAS. However, since its default output, which words are separated by one space, is not convenient for being processed by the NER component. A modification on the output is applied to replace spaces by newline characters. The pseudocode of the implementation is showed in algorithm \ref{code:wst}.

\begin{algorithm}[!t]
\begin{algorithmic}
 \If{user dictionary is not loaded} \State $ictclas\gets ictclas.load\_dict()$ \EndIf
 \If{$text$ is not null}
  \State $result\gets ictclas.process(text)$
  \State $result\gets result.replace(space, newline)$
 \EndIf
 \State \Return $result$
\end{algorithmic}
\caption{Word Segmentation and Tagging}
\label{code:wst}
\end{algorithm}

The sample input \ref{sin:raw} is a original data item of a person. After processed by the way algorithm \ref{code:wst} shows, the text is segmented as the one showed in sample output \ref{sou:seg}. 
\begin{sampleinput}\label{sin:raw}
个人简介: 马欣川，男。资深职业顾问、心理学博士、人才测评博士后、原华南师范大学人力资源研究所副所长、国内最具实战经验的人才测评专家之一。现任职于深圳大学。
\end{sampleinput}

\begin{sampleoutput}\label{sou:seg}
个人/n  简介/n  :/wp  马欣川/nr  ，/wd  男/nse  。/wj  资深/b  职业/n  顾问/np  、/wn  心理学/nm  博士/nde  、/wn  人才测评/nm  博士后/nde  、/wn  原/b  华南师范大学/nte  人力资源研究所/ntr  副/b  所长/np  、/wn  国内/s  最/d  具/vg  实战/n  经验/n  的/ude1  人才测评/nm  专家/n  之一/rz  。/wj  现/tg  任职/v  于/p  深圳大学/nte  。/wj
\end{sampleoutput}

\section{Name Entity Recognition}\label{sec:ner}
Sequences of segmented and tagged words are subsequently processed by the NER component. The NER component adopts rule-based method which defines the rules by context free grammar. In an NER routine, a sequence of words is first loaded into a tokenizer which tokenizes the words and matches them with corresponding token types by regular expression. Then the tokens are processed by a parser which recognizes name entities among consecutive tokens. The PLY(Python Lex-Yacc)\cite{ply} tool is invoked to implement this component.

\subsection{Context Free Grammar}
A context-free grammar (CFG) is a formal grammar in which production rules are of the form $P \rightarrow r$ where $P$ is a nonterminal symbol and $r$ is a sequence of nonterminals, terminals or empty. 19 nonterminals and 49 terminals defined in this component are listed in Table \ref{tab:nonterminals} and Table \ref{tab:terminals} respectively. To recognize name entities, 149 production rules are defined.

\begin{table}[h]
  \centering
  \begin{tabular}{ l | l | l | l }
text & entity & non-entity & person \\ \hline
address & sex & race & title\\ \hline
degree & major & honor & position\\ \hline
industry & education & lab & company\\ \hline
government & organization & date & error
  \end{tabular}
  \caption{Nonterminals}\label{tab:nonterminals}
\end{table}

\begin{table}[h]
  \centering
  \begin{tabular}{ l | l | l | l }
ADDRESS & PERSON & POSITION & POS-PRE \\ \hline
MAJOR & INDUSTRY & ORG & EDU \\ \hline
LAB & COM & GOV & DATE \\ \hline
RACE & SEX & HONOR & TITLE \\ \hline
DEGREE & ADD-POST & EDU-POST & LAB-POST \\ \hline
COM-POST & MAJOR-POST & HONOR-POST & NOUN \\ \hline
TERM & NOUN-OTHER & DATE-OTHER & SPACE \\ \hline
DIRECTION & VERB & ADJECTIVE & DISTINGUISH \\ \hline
STATUS & PRONOUN & MEASURE & QUANTIFIER \\ \hline
ADVERB & PREPOSITION & CONJUNCTION & PARTICLE \\ \hline
INTERJ & TONE & ONOMATOPOEIA & PREFIX \\ \hline
POSTFIX & STR & PUNCTUATION &  OTHER \\ \hline
NEWLINE
  \end{tabular}
  \caption{Terminals(Token Types)}\label{tab:terminals}
\end{table}

\subsection{Tokenization}
In tokenization process, words are read and matched by regular expressions one by one. For each kind of token defined in Table \ref{tab:terminals}, a regular expression is specified for recognition. The pseudocode code showed in Algorithm \ref{code:token} illustrates the procedure of tokenization.
\begin{algorithm}[h]
\begin{algorithmic}
\State $tokens\_regx\gets Defination\ of\ regular\ expressions\ for\ tokens$
    \For {each $word$ in $wrods$}
        \If {$word$ is matched by $tokens\_regx$}
            \State $map(word, correspond\_token\_type)$
            \State $tokens.add(word)$
        \EndIf
    \EndFor
\State \Return $tokens$
\end{algorithmic}
\caption{Tokenization}
\label{code:token}
\end{algorithm}

Text showed in sample input \ref{sin:btok}, whcih is the output of the segmentation procedure, is a sample input of the tokenizer. After tokenization, a list of tokens is produced. The sample out \ref{sou:atok} illustrates an example of tokens.
\begin{sampleinput}\label{sin:btok}
个人/n  简介/n  :/wp  马欣川/nr  ，/wd  男/nse  。/wj  资深/b  职业/n  顾问/np  、/wn  心理学/nm  博士/nde  、/wn  人才测评/nm  博士后/nde  、/wn  原/b  华南师范大学/nte  人力资源研究所/ntr  副/b  所长/np  、/wn  国内/s  最/d  具/vg  实战/n  经验/n  的/ude1  人才测评/nm  专家/n  之一/rz  。/wj  现/tg  任职/v  于/p  深圳大学/nte  。/wj
\end{sampleinput}

\begin{sampleoutput}\label{sou:atok}
('个人/n', NOUN)  ('简介/n', NOUN)  (':/wp', PUNCTUATION)  ('马欣川/nr', PERSON)  ('，/wd', PUNCTUATION)  ('男/nse', SEX)  ('。/wj', PUNCTUATION)  ('资深/b', POS-PRE)  ('职业/n', NOUN)  ('顾问/np', POSITION)  ('、/wn', PUNCTUATION)  ('心理学/nm', MAJOR)  ('博士/nde', DEGREE)  ('、/wn', PUNCTUATION)  ('人才测评/nm', MAJOR)  ('博士后/nde', DEGREE)  ('、/wn', PUNCTUATION)  ('原/b', POS-PRE)  ('华南师范大学/nte', EDU)  ('人力资源研究所/ntr', LAB)  ('副/b', POS-PRE)  ('所长/np', POSITION)  ('、/wn', PUNCTUATION)  ('国内/s', SPACE)  ('最/d', ADVERB)  ('具/vg', VERB)  ('实战/n', NOUN)  ('经验/n', NOUN)  ('的/ude1', PARTICLE)  ('人才测评/nm', MAJOR)  ('专家/n', NOUN)  ('之一/rz', PRONOUN)  ('。/wj', PUNCTUATION)  ('现/tg', DATE-OTHER)  ('任职/v', VERB)  ('于/p', PREPOSITION)  ('深圳大学/nte', EDU)  ('。/wj', PUNCTUATION)  
\end{sampleoutput}

\subsection{Parser}
Tokens are then passed to the NER Parser which uses LALR\cite{lalr} grammar. Consecutive tokens that make up a name entity are extracted, combined and tagged by tags defined in Table \ref{tab:net}. Other tokens that are not part of any name entity are tagged by "O" which means "Other".

\begin{table}[h]
  \centering
  \begin{tabular}{ l | r }
  \textbf{Entity Type} & \textbf{Tag} \\
  \hline
  Degree & DEG \\
  \hline
  Company & COM \\
  \hline
  Education & EDU \\
  \hline
  Labs & LAB \\
  \hline
  Government & GOV \\
  \hline
  Major & MAJ \\
  \hline
  Position & POS \\
  \hline
  Race & RAC \\
  \hline
  Address & ADD \\
  \hline
  Title & TIT \\
  \hline
  Honor & HON \\
  \hline
  Gender & SEX \\
  \hline
  Date & DAT
  \end{tabular}
  \caption{Name Entity Tags}\label{tab:net}
\end{table}

\setlength{\grammarparsep}{20pt plus 1pt minus 1pt} % increase separation between rules
\setlength{\grammarindent}{5em} % increase separation between LHS/RHS
\renewcommand{\syntleft}{}          % do not display '<' associated with variable, for example <A>
\renewcommand{\syntright}{}         % do not display '>' associated with variable, for example <A>

Typical production rules are showed below.

\paragraph{Address}
\begin{grammar}
<address> $\rightarrow$ ADDRESS
\alt ADDRESS ADD-POST
\alt ADDRESS ADDRESS
\alt ADDRESS ADDRESS NOUN ADD-POST
\alt ADDRESS ADDRESS ADDRESS
\alt ADDRESS ADDRESS ADDRESS ADDRESS ADDRESS
\alt ADDRESS ADDRESS ADDRESS ADDRESS ADDRESS ADDRESS
\end{grammar}


\paragraph{Education}
\begin{grammar}
<education> $\rightarrow$ EDU
\alt EDU NOUN EDU-POST
\alt ADDRESS EDU
\alt ADDRESS NOUN EDU
\alt ADDRESS ADDRESS EDU-POST
\alt ADDRESS ADDRESS NOUN EDU-POST
\alt ADDRESS NOUN EDU-POST
\alt ADDRESS PERSON EDU-POST
\alt NOUN-OTHER EDU-POST
\alt ORG NOUN EDU-POST
\alt NOUN NOUN EDU-POST
\alt NOUN NOUN NOUN EDU-POST
\alt PERSON EDU-POST
\alt ADDRESS TERM EDU
\alt ADDRESS TERM NOUN EDU-POST
\alt ADDRESS ADDRESS ADDRESS EDU
\alt ADDRESS NOUN NOUN EDU-POST
\alt ADDRESS ADJECTIVE ADDRESS NOUN-OTHER EDU
\alt ADDRESS ADDRESS NOUN ADJECTIVE EDU-POST
\alt ADDRESS MEASURE NOUN EDU-POST
\alt PERSON NOUN EDU-POST
\alt NOUN ADDRESS NOUN EDU-POST
\alt NOUN ADJECTIVE NOUN EDU-POST
\alt NOUN EDU-POST
\alt ADDRESS NOUN PERSON EDU-POST
\alt ADDRESS ADJECTIVE NOUN EDU-POST
\alt ADJECTIVE PERSON EDU-POST
\alt ADDRESS MEASURE EDU-POST
\alt PERSON NOUN NOUN EDU-POST
\alt MAJOR EDU-POST
\end{grammar}

\paragraph{Lab}
\begin{grammar}
<lab> $\rightarrow$ LAB
\alt ADDRESS ADDRESS NOUN ADJECTIVE NOUN LAB-POST
\alt ADDRESS NOUN NOUN LAB-POST
\alt ADDRESS NOUN LAB-POST
\alt NOUN LAB-POST
\alt ADDRESS ADJECTIVE NOUN LAB-POST
\alt ORG NOUN LAB-POST
\alt NOUN NOUN LAB-POST
\alt ADDRESS ADDRESS NOUN NOUN NOUN LAB-POST
\alt ADDRESS ADDRESS NOUN LAB-POST
\alt ADDRESS NOUN PARTICLE NOUN NOUN NOUN LAB-POST
\alt MEASURE NOUN LAB-POST
\alt ADDRESS NOUN MAJOR NOUN LAB-POST
\alt PERSON LAB-POST
\alt ADDRESS MEASURE NOUN LAB-POST
\alt ORG ADDRESS NOUN-OTHER LAB-POST
\alt MAJOR LAB-POST
\alt NOUN STR LAB-POST
\end{grammar}

\paragraph{Company}
\begin{grammar}
<company> $\rightarrow$ COM
\alt NOUN-OTHER NOUN COM-POST
\alt ADDRESS NOUN COM-POST
\alt ADDRESS ADDRESS NOUN COM-POST
\alt ADDRESS NOUN NOUN-OTHER NOUN-OTHER COM-POST
\alt ADDRESS NOUN NOUN-OTHER NOUN NOUN COM-POST
\alt ADDRESS NOUN NOUN COM-POST
\alt ADDRESS ADDRESS NOUN NOUN COM-POST
\alt ADDRESS PERSON COM-POST
\alt ADDRESS NOUN COM
\alt PERSON NOUN NOUN COM-POST
\alt ADDRESS ADJECTIVE ADJECTIVE NOUN COM-POST
\alt TERM COM-POST
\alt TERM NOUN COM-POST
\alt MEASURE NOUN COM-POST
\alt ADDRESS MEASURE ADJECTIVE COM-POST
\alt ADDRESS TERM NOUN COM-POST
\alt ADDRESS TERM NOUN NOUN COM-POST
\alt TERM ADDRESS COM-POST
\alt ADDRESS ADDRESS NOUN NOUN NOUN COM-POST
\end{grammar}

Text showed in sample input \ref{sin:bpar}, whcih is the output of the tokenization procedure, is the input of the parser. After processed by the parser, a list of tokens tagged with NETs is produced. The sample out \ref{sou:apar} demostrates an sample result.
\begin{sampleinput}\label{sin:bpar}
('个人/n', NOUN)  ('简介/n', NOUN)  (':/wp', PUNCTUATION)  ('马欣川/nr', PERSON)  ('，/wd', PUNCTUATION)  ('男/nse', SEX)  ('。/wj', PUNCTUATION)  ('资深/b', POS-PRE)  ('职业/n', NOUN)  ('顾问/np', POSITION)  ('、/wn', PUNCTUATION)  ('心理学/nm', MAJOR)  ('博士/nde', DEGREE)  ('、/wn', PUNCTUATION)  ('人才测评/nm', MAJOR)  ('博士后/nde', DEGREE)  ('、/wn', PUNCTUATION)  ('原/b', POS-PRE)  ('华南师范大学/nte', EDU)  ('人力资源研究所/ntr', LAB)  ('副/b', POS-PRE)  ('所长/np', POSITION)  ('、/wn', PUNCTUATION)  ('国内/s', SPACE)  ('最/d', ADVERB)  ('具/vg', VERB)  ('实战/n', NOUN)  ('经验/n', NOUN)  ('的/ude1', PARTICLE)  ('人才测评/nm', MAJOR)  ('专家/n', NOUN)  ('之一/rz', PRONOUN)  ('。/wj', PUNCTUATION)  ('现/tg', DATE-OTHER)  ('任职/v', VERB)  ('于/p', PREPOSITION)  ('深圳大学/nte', EDU)  ('。/wj', PUNCTUATION)  
\end{sampleinput}

\begin{sampleoutput}\label{sou:apar}
个人/n/O  简介/n/O  :/wp/O  马欣川/nr/PER  ，/wd/O  男/nse/SEX  。/wj/O  资深职业职业/np/POS  、/wn/O  心理学/nm/MAJ  博士/nde/DEG  、/wn/O  人才测评/nm/MAJ  博士后/nde/DEG  、/wn/O  原/b/O  华南师范大学/nte/EDU  人力资源研究所/ntr/LAB  副所长/np/POS  、/wn/O  国内/s/O  最/d/O  具/vg/O  实战/n/O  经验/n/O  的/ude1/O  人才测评/nm/MAJ  专家/n/O  之一/rz/O  。/wj/O  现/tg/O  任职/v/O  于/p/O  深圳大学/nte/EDU  。/wj/O  
\end{sampleoutput}

\subsection{Date Normalization}
There are various formats of date in the original data, which lead to formats of date in the result of NER parser has no uniform representation, therefore a normalization is necessary for the further processing. The original formats and corresponding normalized formats are showed in Table \ref{tab:dfn}.

\begin{table}[h]
  \centering
  \begin{tabular}{ l | r }
  \textbf{Original Format(after NER parser)} & \textbf{Normalized Format} \\
  \hline
  1990年/t::07月/t::25日/t/DAT & 1990年07月25日/t/DAT\\
  \hline
  1954年/t::1月/t/DAT & 1954年1月/t/DAT \\
  \hline
  一九三七年/t/DAT & 1937年/t/DAT \\
  \hline
  1982.3-1985.1/m/DAT & 1982年3月-1985年1月/t/DAT \\
  \hline
  2000.3/m/DAT & 2000年3月/t/DAT \\
  \hline
  15301603年/t/DAT & 1530年-1603年/t/DAT \\
  \hline
  1998.062003.04/m/DAT & 1998年06月-2003月04日/t/DAT \\
  \hline
  1967.9.22/m/DAT & 1967年9月22日/t/DAT \\
  \hline
  2004-2005/m/DAT & 2004年-2005年/t/DAT \\
  \end{tabular}
  \caption{Date Format Normalization}\label{tab:dfn}
\end{table}

After normalization, the whole NER process is done, and the output of it is passed to the ERR component for furthur processing.

\section{Entity Relation Recognition}\label{sec:err}
The ERR component takes output of the NER component as input and uses regular templates to match sequences of tokens that contain pairs of connected name entities. A regular templates contains one regular expression that can recognize one kind of entity relation in sentences in various grammatical structure. Approximately 28 regular templates are defined to accomplish this task.

\subsection{Relation Types}
Relation types that can be recognized in this component are listed in Table \ref{tab:reltype}.
\begin{table}[h]
  \centering
  \begin{tabular}{ l }
  Person - Gender \\
  \hline
  Person - Nationality \\
  \hline
  Person - Nickname \\
  \hline
  Person - Used name \\
  \hline
  Person - Current address \\
  \hline
  Person - Birthplace \\
  \hline
  Person - Deathday \\
  \hline
  Person - Constellation \\
  \hline
  Person - Race \\
  \hline
  Person - Education(School, Period, Degree) \\
  \hline
  Person - Work experience(Company, Period, Position) \\
  \end{tabular}
  \caption{Entity Relation Types}\label{tab:reltype}
\end{table}

\subsection{Templates}
Templates are consists of units of regular expression which are used to match simple name entities and other feature words of relations. Table \ref{tab:unitregx} shows 37 units of regular expression are defined in current implementation.

\begin{table}[h]
  \centering
  \begin{tabular}{ l | l | l }
PERSON  &  SEX  &  SEX-PREV \\ \hline 
NATION-PREV  &  NAME-PREV  &  NICKNAME-PREV \\ \hline 
USEDNAME-PREV  &  CURRENT-ADD-PREV  &  BIRTHPLACE-PREV \\ \hline 
BWH-PREV  &  BIRTHDAY-PREV  &  BIRTHDAY-VERB \\ \hline 
DEADTIME-PREV  &  DEADTIME-VERB  &  CONSTELLATION-PREV \\ \hline 
CONSTELLATION  &  RACE  &  RACE-PREV \\ \hline 
DATE  &  DATE-POST  &  TIME-FEATURE \\ \hline 
SCHOOL  &  MAJOR  &  DEGREE \\ \hline 
ORG  &  COMPANY  &  LAB \\ \hline 
POSITION  &  TITLE  &  ADDRESS \\ \hline 
SPACE  &  ANY-WORDS  &  EDU-PREV \\ \hline 
EDU-VERBS  &  EDU-POST  &  WORK-PREV \\ \hline 
WORK-VERB
  \end{tabular}
  \caption{Unites of Regular Expression}\label{tab:unitregx}
\end{table}

$Person-Education$ and $Person-Work Experience$ are two typical relations in a person's information. The following are the templates designed for recognizing these kinds of relation from the text.

\paragraph{Templates for Person - Education relation}
\begin{grammar}
<Education> ::= DATE	SCHOOL	*\{0,3\}	MAJOR	*\{0,5\}	(DEGREE|EDU-POST)
\alt    DATE	EDU-VERB	*\{0,3\}	MAJOR	*\{0,5\}	(DEGREE|EDU-POST)
\alt    SCHOOL	*\{0,3\}	MAJOR	*\{1\}	DEGREE
\alt    EDU-PREV	*\{0,5\}	SCHOOL	*?	MAJOR?	*?	DEGREE?
\alt    SCHOOL	*\{1\}	DEGREE
\alt    MAJOR	*\{1\}	DEGREE
\end{grammar}

\paragraph{Templates for Person - Work Experience relation}
\begin{grammar}
<Work> ::= WORK-PREV?	(SCHOOL|COMPANY|ORG|LAB)\{1\}	*\{0,4\}	LAB?	*\{0,3\}	(POSITION|TITLE)
\alt    TIME-FEATURE	(SCHOOL|COMPANY|ORG|LAB)\{1\}	*\{0,4\}	(POSITION|TITLE)
\alt    DATE	(SCHOOL|COMPANY|ORG|LAB)\{1\}	*\{0,4\}	LAB?	*\{0,3\}	(POSITION|TITLE)
\alt    DATE	(SCHOOL|COMPANY|ORG|LAB)\{1\}	DATE-POST	*\{0,4\}	LAB?	*\{0,3\}	(POSITION|TITLE)
\alt    MAJOR	POSITION
\alt    WORK-VERB	(SCHOOL|COMPANY|ORG|LAB)
\end{grammar}

the output of the NER component are divided into a list of sentences, and one of them is processed under multiple templates in the ERR component each time. The algorithm \ref{code:err} shows the way of recognizing person-education and person-work experience relations in one sentence. 
\begin{algorithm}[h]
\begin{algorithmic}
\State $sentence\gets$ A sentence of tokens from NER component
\State $units\_regx\gets$ Defination of units of regular expression
\State $edu\_templates\gets$ Person-education templates
\State $work\_templates\gets$ Person-work experience templates
\State $edu\gets <>$
\State $work\gets <>$
\For {each $phrase$ in $sentence$ match by $edu\_templates$}
    \State $edu.append(phrase.get\_name\_entities())$
\EndFor
\For {each $phrase$ in $sentence$ match by $work\_templates$}
    \State $work.append(phrase.get\_name\_entities())$
\EndFor
\State \Return $edu, work$
\end{algorithmic}
\caption{Entity Relation Recognition}
\label{code:err}
\end{algorithm}

Here are examples of input and output of this module.
\begin{sampleinput}
个人/n/O  简介/n/O  :/wp/O  马欣川/nr/PER  ，/wd/O  男/nse/SEX  。/wj/O
\end{sampleinput}

\begin{sampleoutput}
(SEX, [gender,'男'])
\end{sampleoutput}

\begin{sampleinput}
资深职业职业/np/POS  、/wn/O  心理学/nm/MAJ  博士/nde/DEG  、/wn/O  人才测评/nm/MAJ  博士后/nde/DEG  、/wn/O  原/b/O  华南师范大学/nte/EDU  人力资源研究所/ntr/LAB  副所长/np/POS  、/wn/O  国内/s/O  最/d/O  具/vg/O  实战/n/O  经验/n/O  的/ude1/O  人才测评/nm/MAJ  专家/n/O  之一/rz/O  。/wj/O
\end{sampleinput}

\begin{sampleoutput}
(EDU, [major, '心理学'], [degree, '博士']), (EDU, [major, '人才测评'], [degree, '博士后']), (WORK, [organization, '华南师范大学人力资源研究所'], [position, '副所长'])
\end{sampleoutput}


\section{Duplicates Elimination}\label{sec:dei}
Duplicates are possibly existed among Baidu and Hudong encyclopedias. Therefore, after extraction, a duplicates elimination routine should be executed. The data of a same person may existed in both encyclopedias and the extracted results are partly different; the number of extracted name entities and entity relations are different in that two data items. To deal with data with this feature, the decision tree model is appropriate.

The whole process is listed as follow, corresponding details are described in the subsequent subsections.
\begin{algorithm}[h]
\begin{algorithmic}
\For {each $b\_person$ in $baidu$}
    \For {each $h\_person$ in $hudong.find( b\_person.get\_name() )$}
        \State $cos\_simi\gets get\_cos\_simi(b\_person, h\_person)$
        \If {$bp\_tree.is\_same(cos\_simi) == true$}
            \State $integrate(b\_person, h\_person)$
        \EndIf
    \EndFor
\EndFor
\end{algorithmic}
\caption{Duplicates Detection and Elimination}
\label{code:token}
\end{algorithm}

\subsection{Feature}
The extracted data is organized in a key-value format. Each data item of a person consists of mutiple key-value pairs. Since the number of name entities relation in a text is not determinate, the number of key-value pairs are not consistent. Table \ref{tab:byangjie} and table \ref{tab:hyangjie} shows extracted data of same person from two encyclopedias.

\begin{table}[h]
  \centering
  \begin{tabular}{ l | l }
  data & here
  \end{tabular}
  \caption{Data of 杨捷 Extracted From Baidu Encyclopedia}\label{tab:byangjie}
\end{table}

\begin{table}[h]
  \centering
  \begin{tabular}{ l | l }
  data & here
  \end{tabular}
  \caption{Data of 杨捷 Extracted From Hudong Encyclopedia}\label{tab:hyangjie}
\end{table}

\subsection{Cosine Similarity}
Cosine similarity is invoked to compute the similarity of the same attributes from two data items with same name. Attributes taken to compute include: workExperience, educations, address, gender, birthPlace, birthday, race, height, weight, constellation. -1 is taken to represent the similarity of a specific attribute when both two data items do not have that attribute. If one of the data items do not hold a specific attribute while the other one has, then 0 is assigned as similarity for that attribute. The more similar the two values of a specific attribute, the cosine similarity is more closed to 1. The table illustrats the cosine similarity of data items in \ref{tab:byangjie} and table \ref{tab:hyangjie}.

\begin{table}[h]
  \centering
  \begin{tabular}{ l | r }
  \textbf{Key} & \textbf{Similarity} \\ \hline
  birthPlace & -1 \\ \hline
  race & -1 \\ \hline
  weight & -1 \\ \hline
  gender & 0 \\ \hline
  educations & 0.623302291931 \\ \hline
  birthday & 0 \\ \hline
  workExperience & 0.29983347209375 \\ \hline
  address & -1 \\ \hline
  height & -1
  \end{tabular}
  \caption{Cosine Similarity of Data items in Table and Table}\label{tab:cossimi}
\end{table}

\subsection{Decision Tree}
The problem of judging wether two data items are representing the same person according to the computed cosine similarities is essentially a classfication problem. Decision tree is a model which is qualified to accomplish this task. To apply decision tree, firstly a training process is required. After the training routine, a model is created and it is invoked to classify cosine similarities into two classes: 1. the same, 2. not the same or can not judge. The package scikit-learn\cite{} is used to train and apply decision tree.
\paragraph{Training}
100 groups of randomly chosen cosine similarities computed from 100 pairs data items are taken to training the decision tree model. If the cosine similarity represents the actually same person, then the sample is labled with $1$, otherwise is labled with $0$. 11 samples out of 100 is assigned with $1$ and the others are marked with $0$.

The figure \ref{fig:dtree} shows the produced decision tree after training.
\begin{figure} [!t]
\centering
\includegraphics[height=1\textwidth,width=1\textwidth]{figures/tree.eps}
\caption{Decision Tree} \label{fig:dtree}
\end{figure}

\paragraph{Classification}
The model produced by training is invoked to classify cosine similarities. 
\
