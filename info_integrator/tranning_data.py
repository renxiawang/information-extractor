#!/usr/bin/python
# -*- coding: utf-8 -*-

import pymongo
from pymongo import Connection

# connect the database
db_conn = Connection('127.0.0.1', 27017)
db = db_conn['search_people']
traning_data = db['traning_data']

# define output file
samples_file = open('train_data.txt', 'w')

# get traning data
samples = traning_data.find()

# extract cos simi
for sample in samples:
	samples_file.write(str(sample['_id']) + ',')
	for x in sample['result'].values():
		samples_file.write(str(x) + ',')
	samples_file.write('\n')

samples_file.close()