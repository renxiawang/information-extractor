#!/usr/bin/python
# -*- coding: utf-8 -*-

import types
import sys
import pymongo
import cosine_similarity as cs
import trainer
import json
from bson import json_util

from sklearn import tree
from StringIO import StringIO
from pymongo import Connection

class InfoIntegrator():
    def __init__(self):
        pass

    def to_text(self, dic):
        text = ''
        for x in dic:
            if type(x) == types.DictType:
                text = text + ''.join(x.values())
            else:
                for y in x:
                    text = text + ''.join(y.values())
        return text

    def get_cos_simi(self, target, source):
        '''
        Cosine similarity
        Compute cosine similarity between fields in target and source
        Return a list of cosine similarity value for prediction
        '''
        cos_simi = {}
        attrs = ['workExperience', 'educations', 'address', 'gender', 'birthPlace', 'birthday', 'race', 'height', 'weight', 'constellation']

        for attr in attrs:
            # combine text in workExperience or educations into a string
            if target.has_key(attr) == False and source.has_key(attr) == True:
                if attr == 'workExperience' or attr == 'educations':
                    cos_simi[attr] = 0
                else:
                    cos_simi[attr] = 0
            elif target.has_key(attr) == True and source.has_key(attr) == False:
                if attr == 'workExperience' or attr == 'educations':
                    cos_simi[attr] = 0
                else:
                    cos_simi[attr] = 0
            elif target.has_key(attr) == False and source.has_key(attr) == False:
                cos_simi[attr] = -1
            else:
                if attr == 'workExperience' or attr == 'educations':
                    cos_simi[attr] = cs.get_similarity(self.to_text(target[attr]), self.to_text(source[attr]))
                else:
                    cos_simi[attr] = cs.get_similarity(target[attr], source[attr])

        return cos_simi

    def integrate(self, baidu_per, hudong_per):
        '''
        Combine target and source
        Return a new dict
        '''
        baidu_id = baidu_per['_id']
        hudong_id = hudong_per['_id']

        res = baidu_per.copy()
        for k, v in hudong_per.items():
            if k not in baidu_per or len(json.dumps(baidu_per[k], default=json_util.default)) < len(json.dumps(hudong_per[k], default=json_util.default)):
                res[k] = v

        res['baidu_id'] = baidu_id
        res['hudong_id'] = hudong_id

        return res

def main():
    ii = InfoIntegrator()
    btrainer = trainer.Trainer()
    btree = btrainer.train('train_data.txt')
    out = StringIO()
    out = tree.export_graphviz(btree, out_file=out)
    f = open('tree.dot', 'w')
    f.writelines(out.getvalue())
    f.close()

    # connect db
    db_conn = Connection('127.0.0.1', 27017)
    db = db_conn['renwu_fyp']
    baidu = db['baidu_extracted']
    hudong = db['hudong_extracted']
    combined = db['baidu_hudong']
    # result = db['cos']

    baidu_peop = baidu.find().sort("name", -1)
    attrs = ['workExperience', 'educations', 'address', 'gender', 'birthPlace', 'birthday', 'race', 'height', 'weight', 'constellation']
    
    i = 0
    j = 0

    # find people with same name
    for baidu_per in baidu_peop:
        for hudong_per in hudong.find({"name" : baidu_per['name']}).sort("name", -1):
            i = i + 1
            print 'same name ', i
            cos_simi = ii.get_cos_simi(baidu_per, hudong_per).values()
            if btree.predict(cos_simi)[0] == 1:
                j = j + 1
                print 'duplicates ', j

                combined_person = ii.integrate(baidu_per, hudong_per)
                combined.insert(combined_person)
                break
            else:
                break

if __name__ == '__main__':
    main()