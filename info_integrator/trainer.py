#!/usr/bin/python
# -*- coding: utf-8 -*-
from sklearn import tree

class Trainer():
	def __init__(self):
		self.S = []
		self.L = []
		self.dtree = tree.DecisionTreeClassifier()

	def load_train_data(self, dataset_path):
		dataset = open(dataset_path, 'r')

		for line in dataset.readlines():
			feature = [float(x) for x in line.split(',')[1:-1]]
			self.S.append(feature)
			lable = int(line.split(',')[-1])
			self.L.append(lable)

	def train(self, dataset_path):
		self.load_train_data(dataset_path)
		self.dtree = self.dtree.fit(self.S, self.L)
		return self.dtree