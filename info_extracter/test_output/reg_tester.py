#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

def build_edu_templates():
    global edu_templates_str
    edu_templates = []
    # DATE SCHOOL *{0,3} MAJOR *{0,5} (DEG|EDU_POST)
    edu_templates.append(date + space\
            + school + any_words + r'{0,3}'\
            + major + any_words + r'{0,5}'\
            + '(' + degree + r'|' + edu_post + ')' + r'|')

    # DATE EDU_VERB *{0,3} MAJOR *{0,5} (DEG|EDU_POST)
    edu_templates.append(date + space + edu_verbs\
            + space + school + any_words + r'{0,3}'\
            + major + any_words + r'{0,5}'\
            + '(' + degree + r'|' + edu_post + ')' + r'|')

    # SCHOOL *{0,3} MAJOR *{1} DEGREE
    edu_templates.append(school + space + any_words + r'{0,3}'\
    		+ major + any_words + r'{1}'\
    		+ degree + r'|')

    # EDU_PREV *{0,5} SCHOOL *? MAJOR? *? DEGREE?
    edu_templates.append(edu_prev + space + any_words + r'{0,5}'\
    		+ school + any_words + r'?'\
    		+ major + r'?' + any_words + r'?'\
    		+ degree + r'?' + r'|')

    # SCHOOL *{1} DEGREE
    edu_templates.append(school + any_words + r'{1}'\
    		+ degree + r'|')

    # MAJOR *{1} DEGREE
    edu_templates.append(major + any_words + r'{1}'\
    		+ degree)

    edu_templates_str =  ''.join(edu_templates)

def build_work_templates():
    global work_templates_str
    work_templates = []
    # WORK_FEATURE_WORDS? (EDU|COM|ORG|LAB){1} *{0,4} LAB? *{0,3} (POS|TIT)
    work_templates.append(work_prev + r'?' + space + r'?'\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # TIME_FEATURE (EDU|COM|ORG|LAB){1} *{0,4} (POS|TIT)
    work_templates.append(time_feature + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # DATE (EDU|COM|ORG|LAB){1} *{0,4} LAB? *{0,3} (POS|TIT)
    work_templates.append(date + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # DATE (EDU|COM|ORG|LAB){1} DATE_POST *{0,4} LAB? *{0,3} (POS|TIT)
    work_templates.append(date + space + date_post + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # MAJ POS
    work_templates.append(major + space + position + r'|')

    # WORK_VERB (EDU|COM|ORG|LAB) 
    work_templates.append(work_verb + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + r'|' + any_words + '){1,4}')

    work_templates_str = ''.join(work_templates)

def record_education():
    edu = {}
    for token in match.group().split(' '):
        if token.find('DAT'):
            edu['period'] = token
            continue
        if token.find('EDU'):
            edu['school'] = token
            continue
        if token.find('MAJ'):
            edu['major'] = token
            continue
        if token.find('DEG'):
            edu['degree'] = token
            continue
        # no description here
    return edu

def record_work():
    work = {}
    for token in match.group().split(' '):
        if token.find('DAT'):
            work['period'] = token
            continue
        if token.find('ADD'):
            work['address'] = token
            continue
        if token.find('EDU') or token.find('COM') or token.find('LAB') or token.find('ORG'):
            work['organization'] = work['organization'] + '\n' + token
            continue
        if token.find('POS') or token.find('TIT'):
            work['position'] = work['position'] + '\n' + token
            continue
        # no description here
        # no salary here
    return work

def entity_relation(line):
    global num_line
    num_line = num_line + 1

    for match in re.finditer(edu_templates_str, line):
        if match:
            edu_file.write("%-10d%s\n" % (num_line, match.group().strip()))
            
    for match in re.finditer(work_templates_str, line):
        if match:
            work_file.write("%-10d%s\n" % (num_line, match.group().strip()))

# expressions


date                = r"(\b[0-9.年月日\-/mt]*/DAT\b)"
date_post           = r"(至/p/O\s{1}今/tg/O)"
time_feature        = r"((现/tg/O\s{1}为/p/O)|现任/v/O)"
school              = r"([^\u4e00-\u9fa5^\s{1}]+?/nte/EDU)"
major               = r"([^\u4e00-\u9fa5^\s{1}]+/nm/MAJ)"
degree              = r"([^\u4e00-\u9fa5^\s{1}]+/nde/DEG)"
org                 = r"([^\u4e00-\u9fa5^\s{1}]+/nt/ORG)"
company             = r"([^\u4e00-\u9fa5^\s{1}]+/ntc/COM)"
lab                 = r"([^\u4e00-\u9fa5^\s{1}]+/ntr/LAB)"
position            = r"([^\u4e00-\u9fa5^\s{1}]+/np/POS)"
title               = r"([^\u4e00-\u9fa5^\s{1}]+/nti/TIT)"
address             = r"([^\u4e00-\u9fa5^\s{1}]+/ns[f]?/ADD)"
space               = r"\s{1}"
any_words           = r"([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1})"
edu_prev            = r"((教育/vn/O\s{1}经历/n/O)|(毕业/v/O\s{1}院校/n/O)|(大学/n/O|小学/n/O|中学/n/O|学历/n/O|高中/n/O))"
edu_verbs           = r"(在/p/O|(毕业/v/O\s{1}于/p/O)|考入/v/O)"
edu_post            = r"(研究生/n/O|学习/v/O)"

work_prev           = r"(前/b/O|原/b/O)"
work_verb           = r"((发起/v/O\s{1}组织/n/O)|参与/v/O\s{1}创办/v/O)"

# templates
edu_templates_str = ''
work_templates_str = ''

# temp record result
edu_file  = open('test.edu.txt', 'w')
work_file = open('test.work.txt', 'w')

# line number
num_line = 0

build_edu_templates()
build_work_templates()

f = open('text_file.txt', 'r')
for line in f.readlines():
    entity_relation(line)