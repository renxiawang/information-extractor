#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

def buid_sex_templates():
    '''
        Sex templates
        template 1: 
        template 2: 
        template 3: 
        number of match in 1000 people: 269
    '''
    global sex_templates_str
    sex_templates = []
    # 190 ([^\u4e00-\u9fa5^\s{1}]+/nr[f]?/PER)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,4}((男|女)/nse/SEX)
    sex_templates.append(person + any_words + r'{0,5}' + sex + r'|')

    # 16 (性别/n/O)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}((男|女)/nse/SEX)
    sex_templates.append(sex_prev + any_words + r'{0,3}' + sex + r'|')

    # 36 ((男|女)/nse/SEX)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,5}(\b[0-9.年月日\-/mt]*/DAT\b)
    sex_templates.append(sex + any_words + r'{0,5}' + date)

    sex_templates_str = ''.join(sex_templates)

def buid_nationality_templates():
    '''
        Nationality templates
        template 1: 
        template 2: 
        number of match in 1000 people: 20
    '''
    global nationality_templates_str
    nationality_templates = []
    # 12 (国籍/n/O)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,5}([^\u4e00-\u9fa5^\s{1}]+/ns[f]?/ADD)
    nationality_templates.append(nation_prev + any_words + r'{0,5}' + address + r'|')
    # 0 (\b[0-9.年月日\-/mt]*/DAT\b)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}(入|加入)([^\u4e00-\u9fa5^\s{1}]+/ns[f]?/ADD)+([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}(国籍/n/O)
    nationality_templates.append(date + space + r'(入/v/O|加入/v/O)' + address + space + nation_prev)

    nationality_templates_str = ''.join(nationality_templates)

def build_nickname_templates():
    '''
        Nickname templates
        template 1: 
        number of match in 1000 people: 4
    '''
    global nickname_templates_str
    nickname_templates = []
    # 3 (别名/n/O|绰号/n/O|外号/n/O|艺名/n/O|昵称/n/O|(昵/vg/O\s{1}称/v/O))([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}([^\u4e00-\u9fa5^\s{1}]+/nr[f]?[0-9]?/PER)
    nickname_templates.append(nickname_prev + any_words + r'{0,3}' + person)

    nickname_templates_str = ''.join(nickname_templates)

def build_usedname_templates():
    '''
        Usedname templates
        template 1: 
    '''
    global usedname_templates_str
    usedname_templates = []
    # 21
    usedname_templates.append(usedname_prev + any_words + r'{0,3}' + person)

    usedname_templates_str = ''.join(usedname_templates)  

def build_current_add_templates():
    '''
        Current Address templates
        template 1: 
    '''
    global current_add_templates_str
    current_add_templates = []
    # 6 (现居地/n/O|(现/tg/O\s{1}居/v/O)|(居住/v/O\s{1}城市/n/O))([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}([^\u4e00-\u9fa5^\s{1}]+/ns[f]?/ADD)
    current_add_templates.append(current_add_prev + any_words + r'{0,3}' + address)

    current_add_templates_str = ''.join(current_add_templates)

def build_birthplace_templates():
    '''
        Birthplace templates
        template 1: 
    '''
    global birthplace_templates_str
    birthplace_templates = []
    # 77 
    birthplace_templates.append(birthplace_prev + any_words + r'{0,3}' + address)

    birthplace_templates_str = ''.join(birthplace_templates)

def build_bwh_templates():
    '''
        BWH templates
        template 1: 
    '''
    global bwh_templates_str
    bwh_templates = []
    # 102 (身高/n/O|体重/n/O|三围/n/O)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}[\d.\w]+(/x/O|/m/O)
    bwh_templates.append(bwh_prev + any_words + r'{0,3}' + r'[\d.\w]+(/x/O|/m/O)')

    bwh_templates_str = ''.join(bwh_templates)

def build_birthday_templates():
    '''
        Birthday templates
        template 1: 
        template 2: 
        number of match in 1000 people: 533
    '''
    global birthday_templates_str
    birthday_templates = []
    # 4 (生日/n/O|(出生/vi/O\s{1}日期/n/O)|(出生/vi/O\s{1}年月/n/O))([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}(\b[0-9.年月日\-/mt]*/DAT\b)
    birthday_templates.append(birthday_prev + any_words + r'{0,3}' + date + r'|')
    # 445 (\b[0-9.年月日\-/mt]*/DAT\b)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,2}(生/v/O|生于/v/O|出生/vi/O|(出生/vi/O\s{1}于/p/O))
    birthday_templates.append(date + any_words + r'{0,2}' + birthday_verb)

    birthday_templates_str = ''.join(birthday_templates)

def build_deadtime_templates():
    '''
        Deadline templates
        template 1: 
        template 2: 
        number of match in 1000 people: 23
    '''
    global deadtime_templates_str
    deadtime_templates = []
    # 1 ((逝世/vi/O\s{1}日期/n/O)|(牺牲/vi/O\s{1}日期/n/O)|(死亡/vi/O\s{1}日期/n/O))([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}(\b[0-9.年月日\-/mt]*/DAT\b)
    deadtime_templates.append(deadtime_prev + any_words + r'{0,3}' + date + r'|')
    # 21 (\b[0-9.年月日\-/mt]*/DAT\b)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,8}(逝世/vi/O|牺牲/v/O|死亡/vi/O|死/v/O)
    deadtime_templates.append(date + any_words + r'{0,3}' + deadtime_verb)
    
    deadtime_templates_str = ''.join(deadtime_templates)

def build_constellation_templates():
    '''
        Constellation templates
        template 1: 
    '''
    global constellation_templates_str
    constellation_templates = []
    # (星座/n/O)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}(白羊座/n/O|金牛座/n/O|双子座/n/O|巨蟹座/n/O|狮子座/n/O|处女座/n/O|天秤座/n/O|天蝎座/n/O|射手座/n/O|摩羯座/n/O|水瓶座/n/O|双鱼座/n/O)
    constellation_templates.append(constellation_prev + any_words + r'{0,3}' + constellation)
    
    constellation_templates_str = ''.join(constellation_templates)

def build_race_templates():
    '''
        Race templates
        template 1: 
        number of match in 1000 people: 4
    '''
    global race_templates_str
    race_templates = []
    # (民族/n/O|种族/n/O)([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1}){0,3}([^\u4e00-\u9fa5^\s{1}]+/nra/RAC)
    race_templates.append(race_prev + any_words + r'{0,3}' + race)

    race_templates_str = ''.join(race_templates)
    
def build_edu_templates():
    '''
        Education templates
        template 1: 
        template 2: 
        template 3: 
        template 4: 
        template 5: 
        template 6: 
    '''
    global edu_templates_str
    edu_templates = []
    # DATE SCHOOL *{0,3} MAJOR *{0,5} (DEG|EDU_POST)
    edu_templates.append(date + space\
            + school + any_words + r'{0,3}'\
            + major + any_words + r'{0,5}'\
            + '(' + degree + r'|' + edu_post + ')' + r'|')

    # DATE EDU_VERB *{0,3} MAJOR *{0,5} (DEG|EDU_POST)
    edu_templates.append(date + space + edu_verbs\
            + space + school + any_words + r'{0,3}'\
            + major + any_words + r'{0,5}'\
            + '(' + degree + r'|' + edu_post + ')' + r'|')

    # SCHOOL *{0,3} MAJOR *{1} DEGREE
    edu_templates.append(school + space + any_words + r'{0,3}'\
    		+ major + any_words + r'{1}'\
    		+ degree + r'|')

    # EDU_PREV *{0,5} SCHOOL *? MAJOR? *? DEGREE?
    edu_templates.append(edu_prev + space + any_words + r'{0,5}'\
    		+ school + any_words + r'?'\
    		+ major + r'?' + any_words + r'?'\
    		+ degree + r'?' + r'|')

    # SCHOOL *{1} DEGREE
    edu_templates.append(school + any_words + r'{1}'\
    		+ degree + r'|')

    # MAJOR *{1} DEGREE
    edu_templates.append(major + any_words + r'{1}'\
    		+ degree)

    edu_templates_str =  ''.join(edu_templates)

def build_work_templates():
    '''
        Education templates
        template 1: 
        template 2: 
        template 3: 
        template 4: 
        template 5: 
        template 6: 
    '''
    global work_templates_str

    work_templates = []
    # WORK_FEATURE_WORDS? (EDU|COM|ORG|LAB){1} *{0,4} LAB? *{0,3} (POS|TIT)
    work_templates.append(work_prev + r'?' + space + r'?'\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # TIME_FEATURE (EDU|COM|ORG|LAB){1} *{0,4} (POS|TIT)
    work_templates.append(time_feature + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # DATE (EDU|COM|ORG|LAB){1} *{0,4} LAB? *{0,3} (POS|TIT)
    work_templates.append(date + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # DATE (EDU|COM|ORG|LAB){1} DATE_POST *{0,4} LAB? *{0,3} (POS|TIT)
    work_templates.append(date + space + date_post + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + ')'\
            + any_words + r'{0,4}'\
            + lab + r'?' + any_words + r'{0,4}'\
            + '(' + position + r'|' + title + r'|'\
            + '(' + any_words + '(' + position + '|' + title + ')' + ')+' + ')' + r'|')

    # MAJ POS
    work_templates.append(major + space + position + r'|')

    # WORK_VERB (EDU|COM|ORG|LAB) 
    work_templates.append(work_verb + space\
            + '(' + school + r'|' + company + r'|' + org + r'|' + lab + r'|' + any_words + '){1,4}')

    work_templates_str = ''.join(work_templates)

def record_education(match):
    '''
        Record all education experience
        return a dict
    '''
    edu = {}
    for token in match.split(' '):
        if token.find('DAT') != -1:
            edu['period'] = token.split('/')[0]
            continue
        if token.find('EDU') != -1:
            edu['school'] = token.split('/')[0]
            continue
        if token.find('MAJ') != -1:
            edu['major'] = token.split('/')[0]
            continue
        if token.find('DEG') != -1:
            edu['degree'] = token.split('/')[0]
            continue
        # no description here
    return edu

def record_work(match):
    '''
        Record all work experience
        return a dict
    '''
    work = {}
    for token in match.split(' '):
        if token.find('DAT') != -1:
            work['period'] = token.split('/')[0]
            continue
        if token.find('ADD') != -1:
            work['address'] = token.split('/')[0]
            continue
        if token.find('EDU') != -1 or token.find('COM') != -1 or token.find('LAB') != -1 or token.find('ORG') != -1:
            if work.has_key('organization'):
                work['organization'] = work['organization'] + '\t' + token.split('/')[0]
            else:
                work['organization'] = token.split('/')[0]
            continue
        if token.find('POS') != -1 or token.find('TIT') != -1:
            if work.has_key('position'):
                work['position'] = work['position'] + '\t' + token.split('/')[0]
            else:
                work['position'] = token.split('/')[0]
            continue
        # no description here
        # no salary here
    return work

def record_simple_attrs(attr_type, tag, match, attrs_dict):
    '''
        Record all extracted simple attrs
        return a dict
    '''

    for token in match.split(' '):
        if attr_type == 'BWH':
            pass
        elif token.find(tag) != -1:
            attrs_dict[attr_type] = token.split('/')[0]

    return attrs_dict




def entity_relation(line, existed_simple_attrs, existed_advanced_attrs):
    '''
        Extracting entity relation from each sentence
        return a list contain extraction result
        including edu, work
    '''
    global num_line
    # advanced attrs
    edu = []
    work = []

    # simple attrs
    extracted_simple_attrs = {}
    num_line = num_line + 1
    '''
    Check regular expressions
    '''
    # simple_attrs.writelines( '-----------------------------\nNow Processing:\n' + line + '\nThe Results are:\n')

    # extract advanced arrts
    for match in re.finditer(edu_templates_str, line):
        if match:
            # simple_attrs.write("%-10d%s\n" % (num_line, match.group().strip()))
            edu.append(record_education(match.group()))
            
    for match in re.finditer(work_templates_str, line):
        if match:
            # simple_attrs.write("%-10d%s\n" % (num_line, match.group().strip()))
            work.append(record_work(match.group()))
    # TODO nickname, usedname
    # extract simple attrs
    for (k, v) in existed_simple_attrs.items():
        if k == 'gender' and v == 0:# and v == 0:
            for match in re.finditer(sex_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    extracted_simple_attrs = record_simple_attrs('gender', 'SEX', match.group(), extracted_simple_attrs)
                    break
        if k == 'nationality' and v == 0:# and v == 0:
            for match in re.finditer(nationality_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    extracted_simple_attrs = record_simple_attrs('nationality', 'ADD', match.group(), extracted_simple_attrs)
                    break
        if k == 'nickname' and v == 0:# and v == 0:
            for match in re.finditer(nickname_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    # TO CHECK
                    extracted_simple_attrs = record_simple_attrs('nickname', 'PER', match.group(), extracted_simple_attrs)
                    break
        if k == 'address' and v == 0:# and v == 0:
            for match in re.finditer(current_add_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    extracted_simple_attrs = record_simple_attrs('address', 'ADD', match.group(), extracted_simple_attrs)
                    break
        if k == 'birthPlace' and v == 0:# and v == 0:
            for match in re.finditer(birthplace_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    extracted_simple_attrs = record_simple_attrs('birthPlace', 'ADD', match.group(), extracted_simple_attrs)
                    break
        if k == 'BWH' and v == 0:# and v == 0:
            for match in re.finditer(bwh_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    extracted_simple_attrs = record_simple_attrs('BWH', '/m/O', match.group(), extracted_simple_attrs)
                    break
        if k == 'birthday' and v == 0:# and v == 0:
            for match in re.finditer(birthday_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    # TO NORMALIZATION
                    extracted_simple_attrs = record_simple_attrs('birthday', 'DAT', match.group(), extracted_simple_attrs)
                    break
        if k == 'deadtime' and v == 0:# and v == 0:
            for match in re.finditer(deadtime_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    # TO NORMALIZATION
                    extracted_simple_attrs = record_simple_attrs('deadtime', 'DAT', match.group(), extracted_simple_attrs)
                    break
        if k == 'constellation' and v == 0:# and v == 0:
            for match in re.finditer(constellation_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    # TAG!!!!
                    extracted_simple_attrs = record_simple_attrs('constellation', '座/n/O', match.group(), extracted_simple_attrs)
                    break
        if k == 'race' and v == 0:# and v == 0:
            for match in re.finditer(race_templates_str, line):
                if match:
                    # simple_attrs.writelines("%-10d%s : %s\n" % (num_line, k, match.group().strip()))
                    extracted_simple_attrs = record_simple_attrs('race', 'RAC', match.group(), extracted_simple_attrs)
                    break

    return [edu, work, extracted_simple_attrs]

'''
    Setup global variables
'''
# expressions
person              = r"([^\u4e00-\u9fa5^\s{1}]+/nr[f]?[0-9]?/PER)"
sex                 = r"((男|女)/nse/SEX)"
sex_prev            = r"(性别/n/O)"
nation_prev         = r"(国籍/n/O)"
name_prev           = r"(名字/n/O|中文名/n/O|中文全名/n/O|姓名/n/O)"
nickname_prev       = r"(别名/n/O|绰号/n/O|外号/n/O|艺名/n/O|昵称/n/O|(昵/vg/O\s{1}称/v/O))"
usedname_prev       = r"(原名/n/O|曾用名/n/O)"
current_add_prev    = r"(现居地/n/O|(现/tg/O\s{1}居/v/O)|(居住/v/O\s{1}城市/n/O))"
birthplace_prev     = r"(籍贯/n/O|出生地/n/O|家乡/n/O|故乡/n/O|(出生/vi/O\s{1}(于|在)/p/O))"
bwh_prev            = r"(身高/n/O|体重/n/O|三围/n/O)"
birthday_prev       = r"(生日/n/O|(出生/vi/O\s{1}日期/n/O)|(出生/vi/O\s{1}年月/n/O))"
birthday_verb       = r"(生/v/O|生于/v/O|出生/vi/O|(出生/vi/O\s{1}于/p/O))"
deadtime_prev       = r"((逝世/vi/O\s{1}日期/n/O)|(牺牲/v/O\s{1}日期/n/O)|(死亡/vi/O\s{1}日期/n/O))"
deadtime_verb       = r"(逝世/vi/O|牺牲/v/O|死亡/vi/O|死/v/O)"
constellation_prev  = r"(星座/n/O)"
constellation       = r"(白羊座/n/O|金牛座/n/O|双子座/n/O|巨蟹座/n/O|狮子座/n/O|处女座/n/O|天秤座/n/O|天蝎座/n/O|射手座/n/O|摩羯座/n/O|水瓶座/n/O|双鱼座/n/O)"
race                = r"([^\u4e00-\u9fa5^\s{1}]+/nra/RAC)"
race_prev           = r"(民族/n/O|种族/n/O)"

date                = r"(\b[0-9.年月日\-/mt]*/DAT\b)"
date_post           = r"(至/p/O\s{1}今/tg/O)"
time_feature        = r"((现/tg/O\s{1}为/p/O)|现任/v/O)"
school              = r"([^\u4e00-\u9fa5^\s{1}]+?/nte/EDU)"
major               = r"([^\u4e00-\u9fa5^\s{1}]+/nm/MAJ)"
degree              = r"([^\u4e00-\u9fa5^\s{1}]+/nde/DEG)"
org                 = r"([^\u4e00-\u9fa5^\s{1}]+/nt/ORG)"
company             = r"([^\u4e00-\u9fa5^\s{1}]+/ntc/COM)"
lab                 = r"([^\u4e00-\u9fa5^\s{1}]+/ntr/LAB)"
position            = r"([^\u4e00-\u9fa5^\s{1}]+/np/POS)"
title               = r"([^\u4e00-\u9fa5^\s{1}]+/nti/TIT)"
address             = r"([^\u4e00-\u9fa5^\s{1}]+/ns[f]?/ADD)"
space               = r"\s{1}"
any_words           = r"([^\u4e00-\u9fa5^\s{1}]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|[0-9a-zA-Z：:．,，]+/[a-z0-9]{1,3}/[A-Z]{1,3}\s{1}|\s{1})"
edu_prev            = r"((教育/vn/O\s{1}经历/n/O)|(毕业/v/O\s{1}院校/n/O)|(大学/n/O|小学/n/O|中学/n/O|学历/n/O|高中/n/O))"
edu_verbs           = r"(在/p/O|(毕业/v/O\s{1}于/p/O)|考入/v/O)"
edu_post            = r"(研究生/n/O|学习/v/O)"

work_prev           = r"(前/b/O|原/b/O)"
work_verb           = r"((发起/v/O\s{1}组织/n/O)|参与/v/O\s{1}创办/v/O)"

# templates
sex_templates_str = ''
edu_templates_str = ''
work_templates_str = ''
nationality_templates_str = ''
name_templates_str = ''
nickname_templates_str = ''
usedname_templates_str = ''
current_add_templates_str = ''
birthplace_templates_str = ''
bwh_templates_str = ''
birthday_templates_str = ''
deadtime_templates_str = ''
constellation_templates_str = ''
race_templates_str = ''

# temp record result
#edu_file  = open('test.edu.txt', 'w')
#work_file = open('test.work.txt', 'w')
#simple_attrs = open('simple_attrs.txt', 'w')

# line number
num_line = 0

'''
    Construct templates
'''
build_edu_templates()
build_work_templates()
buid_sex_templates()
buid_nationality_templates()
# build_name_templates()
build_nickname_templates()
build_usedname_templates()
build_current_add_templates()
build_birthplace_templates()
build_bwh_templates()
build_birthday_templates()
build_deadtime_templates()
build_constellation_templates()
build_race_templates()
    
    
