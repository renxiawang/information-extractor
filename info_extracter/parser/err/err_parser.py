#!/usr/bin/python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# err_parser.py
#
# 
# 
# ------------------------------------------------------------

import ply.yacc as yacc

# Get the token map from the lexer.  This is required.
from err_tokenizer import tokens
from err_tokenizer import data

def p_text(t):
    '''text : 
            | relation text
            | PUNCTUATION text
    '''
    t = None

def p_relation_social(t):
    '''relation     : social
                    | location
                    | employment
                    | education
                    | title
                    | race
                    | sex
                    | honor
    '''

def p_social(t):
    '''social   : RELATION_NOUN     PERSON
                | PERSON   RELATION_NOUN 
    '''
    t[0] = t[1] + t[2]

def p_location(t):
    '''location : address   LOCAL_NOUN
    '''
    t[0] = t[1] + t[2]

def p_address(t):
    '''address  : ADDRESS
                | address   ADDRESS
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_date(t):
    '''date : DATE
            | DATE DATE
            | DATE DATE DATE
            | DATE DATE DATE DATE
            | DATE DATE DATE DATE DATE
            | DATE DATE DATE DATE DATE DATE
    '''
    if len(t) == 7:
        t[0] = t[1] + t[2] + t[3] + t[4] + t[5] + t[6]
    elif len(t) == 6:
        t[0] = t[1] + t[2] + t[3] + t[4] + t[5]
    elif len(t) == 5:
        t[0] = t[1] + t[2] + t[3] + t[4]
    elif len(t) == 4:
        t[0] = t[1] + t[2] + t[3]
    elif len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]
    
def p_race(t):
    '''race     : RACE
    '''
    t[0] = t[1]

def p_sex(t):
    '''sex      : SEX
    '''
    t[0] = t[1]

def p_honor(t):
    '''honor    : HONO_VERB honseq
                | date  HONO_VERB honseq
    '''
    if len(t) == 4:
        t[0] = t[1] + t[2] + t[3]
    else:
        t[0] = t[1] + t[2]

def p_honseq(t):
    '''honseq   : HONOR
                | honseq    HONOR
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_title(t):
    '''title    : date  TITLE
                | TITLE
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_employment(t):
    '''employment   : job   EMPL_NOUN
                    | job   EMPL_VERB   position
                    | date  position
                    | EMPL_VERB position
                    | date  EMPL_VERB   ORG
                    | date  job  PUNCTUATION
    '''
    if len(t) == 4:
        t[0] = t[1] + t[2] + t[3]
    else:
        t[0] = t[1] + t[2]

def p_job(t):
    '''job  : organization
            | organization  position
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_position(t):
    '''position     : POSITION
                    | position  POSITION
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_education(t):
    '''education    : edu   DEGREE
                    | date  edu     DEGREE
                    | date  edu     MAJOR
                    | date  edu     MAJOR   DEGREE
                    | date  edu     EDUC_VERB
    '''
    if len(t) == 4:
        t[0] = t[1] + t[2] + t[3]
    else:
        t[0] = t[1] + t[2]

def p_organization(t):
    '''organization     : org
                        | lab
                        | com
                        | gov
    '''
    t[0] = t[1]

def p_org(t):
    '''org  : ORG
            | org ORG
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_edu(t):
    '''edu  : EDU
            | edu EDU
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_lab(t):
    '''lab  : LAB
            | lab LAB
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_com(t):
    '''com  : COM
            | com COM
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_gov(t):
    '''gov  : GOV
            | gov GOV
    '''
    if len(t) == 3:
        t[0] = t[1] + t[2]
    else:
        t[0] = t[1]

def p_error(t):
    # print yacc.token().value
    # print "Syntax error at %s" % t.value
    yacc.restart()


social      = open('/workspace/svn/search/ie/parser/sample_output/err_social.txt', 'w')
location    = open('/workspace/svn/search/ie/parser/sample_output/err_location.txt', 'w')
employment  = open('/workspace/svn/search/ie/parser/sample_output/err_employment.txt', 'w')
education   = open('/workspace/svn/search/ie/parser/sample_output/err_education.txt', 'w')
title       = open('/workspace/svn/search/ie/parser/sample_output/err_title.txt', 'w')
race        = open('/workspace/svn/search/ie/parser/sample_output/err_race.txt', 'w')
sex         = open('/workspace/svn/search/ie/parser/sample_output/err_sex.txt', 'w')
honor       = open('/workspace/svn/search/ie/parser/sample_output/err_honor.txt', 'w')

parser = yacc.yacc(outputdir="./parser/err")