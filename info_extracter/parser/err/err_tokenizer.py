#!/usr/bin/python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# err_tokenizer.py
#
# 
# 
# ------------------------------------------------------------
import ply.lex as lex

# List of token names.   This is always required
tokens = (
    'ADDRESS',
    'PERSON',
    'POSITION',
    'MAJOR',
    'INDUSTRY',
    'ORG',
    'EDU',
    'LAB',
    'COM',
    'GOV',
    'DATE',
    'RACE',
    'SEX',
    'HONOR',
    'TITLE',
    'DEGREE',
    'PUNCTUATION',
    'LOCAL_NOUN',
    'EMPL_VERB',
    'EMPL_NOUN',
    'HONO_VERB',
    'EDUC_VERB',
    'DIE_VERB',
    'BIRTH_VERB',
    'RELATION_NOUN'
)

# Regular expression rules for simple tokens
t_ADDRESS        =  u".+(ADD)(\n)"
t_PERSON         =  u".+(PER)(\n)"
t_POSITION       =  u".+(POS)(\n)"
t_MAJOR          =  u".+(MAJ)(\n)"
t_INDUSTRY       =  u".+(IND)(\n)"
t_ORG            =  u".+(ORG)(\n)"
t_EDU            =  u".+(EDU)(\n)"
t_LAB            =  u".+(LAB)(\n)"
t_COM            =  u".+(COM)(\n)"
t_GOV            =  u".+(GOV)(\n)"
t_DATE           =  u".+(DAT)(\n)"
t_RACE           =  u".+(RAC)(\n)"
t_SEX            =  u".+(SEX)(\n)"
t_HONOR          =  u".+(HON)(\n)"
t_TITLE          =  u".+(TIT)(\n)"
t_DEGREE         =  u".+(DEG)(\n)"
t_PUNCTUATION    =  u"(。|！).+(\n)"
t_LOCAL_NOUN     =  u"(人)(\s){1}.+(\n)"
t_EMPL_VERB      =  u"(现任|担任|加入|创立|出任|的|任).+(\n)"
t_EMPL_NOUN      =  u"(创始人).+(\n)"
t_HONO_VERB      =  u"(获得|授予).+(\n)"
t_EDUC_VERB      =  u"(毕业|就读|入读|辍学|学习).+(\n)"
t_DIE_VERB       =  u"(逝世|去世|死).+(\n)"
t_BIRTH_VERB     =  u"(出生|生).+(\n)"
t_RELATION_NOUN  =  u"(父亲|母亲|女儿|儿子|媳妇|长子|次子|爷爷|祖父|奶奶|祖母|弟弟|妹妹|爸爸|妈妈|妻子).+(\n)"

# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    # print "Illegal character '%s'" % t.value[0]
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()

# Test it out
data = ''''''

# # Give the lexer some input
# lexer.input(data)

# # Tokenize
# # while True:
# #     tok = lexer.token()
# #     if not tok: break      # No more input
# #     print tok
# for tok in iter(lex.token, None):
#     print "%-5d%-20s%-20s" % (tok.lineno, tok.type.encode('utf-8'), tok.value.encode('utf-8'))

