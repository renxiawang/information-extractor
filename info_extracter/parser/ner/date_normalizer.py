#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

def normalizer(date):
    date = date.encode('utf-8')
    if re.match(template_1, date):
        date = date.replace('/t::', '')
    elif re.match(template_2, date):
        date = date.replace('/t::', '')
    elif re.match(template_3, date):
        date = date.replace('/t::', '')
    elif re.match(template_4, date):
        date = date.replace('/t::', '')
        date = date.replace('零', '0')
        date = date.replace('一', '1')
        date = date.replace('二', '2')
        date = date.replace('三', '3')
        date = date.replace('四', '4')
        date = date.replace('五', '5')
        date = date.replace('六', '6')
        date = date.replace('七', '7')
        date = date.replace('八', '8')
        date = date.replace('九', '9')
        date = date.replace('十', '0')
    elif re.match(template_5, date):
        date = date.replace('/t::', '')
        date = date.replace('零', '0')
        date = date.replace('一', '1')
        date = date.replace('二', '2')
        date = date.replace('三', '3')
        date = date.replace('四', '4')
        date = date.replace('五', '5')
        date = date.replace('六', '6')
        date = date.replace('七', '7')
        date = date.replace('八', '8')
        date = date.replace('九', '9')
        date = date.replace('十', '1')
    elif re.match(template_6, date):
        date = date.replace('/t::', '')
        if date.find('二十日') != -1:
            # 二十日 -> 20
            date = date.replace('十', '0')
        elif date.find('三十日') != -1:
            # 三十日 -> 30
            date = date.replace('十', '0')
        elif date.find('十日') != -1:
            # 十日 -> 10
            date = date.replace('十', '10')
        elif date.find('日') - date.find('十') != 1:
            # 十二日 -> 12日
            date = date.replace('十', '1')
        else:
            # 二十八 -> 28
            date = date.replace('十', '')
        date = date.replace('零', '0')
        date = date.replace('一', '1')
        date = date.replace('二', '2')
        date = date.replace('三', '3')
        date = date.replace('四', '4')
        date = date.replace('五', '5')
        date = date.replace('六', '6')
        date = date.replace('七', '7')
        date = date.replace('八', '8')
        date = date.replace('九', '9')

    elif re.match(template_7, date):
        tmp = date.split('.')
        if len(tmp) == 3:
            # 1982.3-1985.1/m/DAT
            year1 = tmp[0] + '年'
            month1 = tmp[1].split('-')[0] + '月'
            year2 = tmp[1].split('-')[1] + '年'
            month2 = tmp[2].split('/')[0] + '月'
            date = year1 + month1 + '-' + year2 + month2 + '/t/DAT'
        else:
            # 2000.3/m/DAT
            year = tmp[0] + '年'
            month = tmp[1].split('/')[0] + '月'
            date = year + month + '/t/DAT'
    elif re.match(template_8, date):
        # 15301603年/t/DAT
        year1 = date[:4] + '年'
        year2 = date[4:]
        date = year1 + year2
    elif re.match(template_9, date):
        # 50年/t/DAT
        pass
    elif re.match(template_10, date):
        # 1998.062003.04/m/DAT
        date1 = date[:7]
        year1 = date1.split('.')[0] + '年'
        month1 = date1.split('.')[1] + '月'
        date2 = date[7:]
        year2 = date2.split('.')[0] + '年'
        month2 = date2.split('.')[1].split('/')[0] + '月'

        date = year1 + month1 + '-' + year2 + month2 + '/t/DAT'
    elif re.match(template_11, date):
        # 1967.9.22/m/DAT
        # 1981.01/m/DAT
        tmp = date.split('.')
        if len(tmp) == 3:
            year = tmp[0] + '年'
            month = tmp[1] + '月'
            day = tmp[2].split('/')[0] + '日'
            date = year + month + day + '/t/DAT'
        else:
            year = tmp[0] + '年'
            month = tmp[1].split('/')[0] + '月'
            date = year + day + '/t/DAT'
    elif re.match(template_12, date):
        # 2004-2005/m/DAT
        year1 = date.split('-')[0] + '年'
        year2 = date.split('-')[1].split('/')[0] + '年'
        date = year1 + '-' + year2 + '/t/DAT'
    else:
        pass

    return date.decode('utf-8')
    #print i, date.strip()

# 1990年/t::07月/t::25日/t/DAT
# 1954年/t::1月/t/DAT
# 11月/t/DAT
template_1 = r"^([0-9]{4}年(\/t::|\/t/DAT))([0-9]{1,2}(月|日)(\/t::|\/t/DAT))*"      # 年(月日)*
template_2 = r"^([0-9]{1,2}(月)(\/t::|\/t/DAT))([0-9]{1,2}(日)(\/t::|\/t/DAT))*"      # 月(日)*
template_3 = r"^([0-9]{1,2}(日)(\/t::|\/t/DAT))"       # 日

# 一九三七年/t/DAT
# 一一年/t::二月/t::十六日/t/DAT
# 二月/t::十六日/t/DAT
# \u96f6\u4e00\u4e8c\u4e09\u56db\u4e94\u516d\u4e03\u516b\u4e5d\u5341
template_4 = r"^((零|一|二|三|四|五|六|七|八|九|十){1,4}年(\/t::|\/t/DAT))((零|一|二|三|四|五|六|七|八|九|十){1,3}(月|日)(\/t::|\/t/DAT))*" # 年(月日)*
template_5 = r"^((零|一|二|三|四|五|六|七|八|九|十){1,3}(月)(\/t::|\/t/DAT))((零|一|二|三|四|五|六|七|八|九|十){1,3}(日)(\/t::|\/t/DAT))*" # 月(日)*
template_6 = r"^((零|一|二|三|四|五|六|七|八|九|十){1,3}(日)(\/t::|\/t/DAT))"


# 1982.3-1985.1/m/DAT
# 2000.3/m/DAT
template_7 = r"^[0-9]{4}\.[0-9]{1,2}(-[0-9]{4}\.[0-9]{1,2})*\/m/DAT"

# 15301603年/t/DAT
template_8 = r"^[0-9]{8}年(/t/DAT)"

# 50年/t/DAT
template_9 = r"^[0-9]{2}年(/t/DAT)"

# 1998.062003.04/m/DAT
# 2000.052000.08/m/DAT
# 2003.042004.03/m/DAT
# 2004.032004.05/m/DAT
template_10 = r"^[0-9]{4}\.[0-9]{6}\.[0-9]{2}\/m/DAT"

# 1967.9.22/m/DAT
# 1981.01/m/DAT
template_11 = r"^[0-9]{4}(\.[0-9]{1,2})+(/m/DAT)"

# 2004-2005/m/DAT
template_12 = r"^[0-9]{4}-[0-9]{4}/m/DAT"

