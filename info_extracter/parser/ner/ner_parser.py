#!/usr/bin/python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# ner_parser.py
#
# 
# 
# ------------------------------------------------------------

import ply.yacc as yacc
import ply.lex as lex
import logging
import date_normalizer

# Get the token map from the lexer.  This is required.
from ner_tokenizer import tokens
from ner_tokenizer import ner_lexer

ner_result = []
pre_token = ''

logging.basicConfig(
    level = logging.DEBUG,
    filename = "./parser/ner/parselog.txt",
    filemode = "w",
    format = "%(filename)10s:%(lineno)4d:%(message)s"
) 

precedence = (
    ('right', 'NOUN', 'EDU', 'ADDRESS', 'STR', 'LAB_POST', 'NOUN_OTHER', 'DATE', 'DATE_OTHER', 'MEASURE', 'PERSON', 'ADJECTIVE', 'TERM', 'COM_POST', 'EDU_POST', 'MAJOR_POST'),
)

def p_text(t):
    '''
    text : 
         | entity text
         | non_entity text
    '''
    t = None

def p_entity(t):
    '''
    entity : person
           | address
           | sex
           | race
           | title
           | degree
           | major
           | honor
           | position
           | industry
           | education
           | lab
           | company
           | government
           | organization
           | date
           | error
    '''
    # print ':++++++++++++++++++++++++++++++++++++++++++++%s' % pre_token[]
    # if isinstance(t[1], lex.LexToken) == True:
    #   print t[1] # .value.encode('utf-8')
    #   print "-----------------------%s" % ner_parser.symstack[-1]
    global pre_token
    if pre_token != '':
        ner_result.append(pre_token + '/O')
        pre_token = ''

def p_non_entity(t):
    '''
    non_entity : EDU_POST
               | MAJOR_POST
               | PUNCTUATION
               | POSTFIX
               | PREFIX
               | ONOMATOPOEIA
               | TONE
               | INTERJ
               | PARTICLE
               | CONJUNCTION
               | PREPOSITION
               | ADVERB
               | QUANTIFIER
               | PRONOUN
               | STATUS
               | DISTINGUISH
               | VERB
               | DIRECTION
               | SPACE
               | DATE_OTHER
               | NOUN
               | NOUN_OTHER
               | MEASURE
               | ADJECTIVE
               | TERM
               | STR
               | OTHER
               | NEWLINE
    '''
    global pre_token
    if t[1] == '\n\n':
      ner_result.append(t[1])
    else:
      ner_result.append(t[1] + '/O')
      if pre_token != '':
        ner_result.append(pre_token + '/O')
        pre_token = ''

def p_person(t):
    '''
    person : PERSON
    '''
    t[0] = t[1] + '/PER'
    ner_result.append(t[0])

def p_address(t):
    '''
    address : ADDRESS
            | ADDRESS ADD_POST
            | ADDRESS ADDRESS
            | ADDRESS ADDRESS NOUN ADD_POST
            | ADDRESS ADDRESS ADDRESS
            | ADDRESS ADDRESS ADDRESS ADDRESS ADDRESS
            | ADDRESS ADDRESS ADDRESS ADDRESS ADDRESS ADDRESS
    '''
    if len(t) == 7:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + t[5].split('/')[0] + t[6].split('/')[0] + '/ns/ADD'
    elif len(t) == 6:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + t[5].split('/')[0] + '/ns/ADD'
    elif len(t) == 5:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + '/ns/ADD'
    elif len(t) == 4:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + '/ns/ADD'
    elif len(t) == 3:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + '/ns/ADD'
    else:
        t[0] = t[1].split('/')[0] + '/ns/ADD'
    ner_result.append(t[0])
    

def p_sex(t):
    '''
    sex : SEX
    '''
    t[0] = t[1] + '/SEX'
    ner_result.append(t[0])
    

def p_race(t):
    '''
    race : RACE
    '''
    t[0] = t[1] + '/RAC'
    ner_result.append(t[0])
    

def p_title(t):
    '''
    title : TITLE
    '''
    t[0] = t[1] + '/TIT'
    ner_result.append(t[0])
    

def p_degree(t):
    '''
    degree : DEGREE
    '''
    t[0] = t[1] + '/DEG'
    ner_result.append(t[0])
    

def p_major(t):
    '''
    major : MAJOR
          | MAJOR MAJOR_POST
          | VERB MAJOR_POST
          | NOUN MAJOR_POST
          | NOUN_OTHER MAJOR_POST
    '''
    if len(t) == 3:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + '/nm/MAJ'
    else:
        t[0] = t[1].split('/')[0] + '/nm/MAJ'
    ner_result.append(t[0])
    

def p_honor(t):
    '''
    honor : HONOR
          | NOUN HONOR_POST
    '''
    t[0] = t[1] + '/HON'
    ner_result.append(t[0])
    

def p_position(t):
    '''
    position : POSITION
             | POS_PRE POSITION
             | POS_PRE NOUN POSITION
    '''
    if len(t) == 4:
      t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[2].split('/')[0] + '/np/POS'
    elif len(t) == 3:
      t[0] = t[1].split('/')[0] + t[2].split('/')[0] + '/np/POS'
    else:
      t[0] = t[1].split('/')[0] + '/np/POS'
    ner_result.append(t[0])
    

def p_industry(t):
    '''
    industry : INDUSTRY
    '''
    t[0] = t[1] + '/IND'
    ner_result.append(t[0])
    

def p_education(t):
    '''
    education : EDU
              | EDU NOUN EDU_POST
              | ADDRESS EDU
              | ADDRESS NOUN EDU
              | ADDRESS ADDRESS EDU_POST
              | ADDRESS ADDRESS NOUN EDU_POST
              | ADDRESS NOUN EDU_POST
              | ADDRESS PERSON EDU_POST
              | NOUN_OTHER EDU_POST
              | ORG NOUN EDU_POST
              | NOUN NOUN EDU_POST
              | NOUN NOUN NOUN EDU_POST
              | PERSON EDU_POST
              | ADDRESS TERM EDU
              | ADDRESS TERM NOUN EDU_POST
              | ADDRESS ADDRESS ADDRESS EDU
              | ADDRESS NOUN NOUN EDU_POST
              | ADDRESS ADJECTIVE ADDRESS NOUN_OTHER EDU
              | ADDRESS ADDRESS NOUN ADJECTIVE EDU_POST
              | ADDRESS MEASURE NOUN EDU_POST
              | PERSON NOUN EDU_POST
              | NOUN ADDRESS NOUN EDU_POST
              | NOUN ADJECTIVE NOUN EDU_POST
              | NOUN EDU_POST
              | ADDRESS NOUN PERSON EDU_POST
              | ADDRESS ADJECTIVE NOUN EDU_POST
              | ADJECTIVE PERSON EDU_POST
              | ADDRESS MEASURE EDU_POST
              | PERSON NOUN NOUN EDU_POST
              | MAJOR EDU_POST
    '''
    if len(t) == 5:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + '/nte/EDU'
    elif len(t) == 4:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + '/nte/EDU'
    elif len(t) == 3:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + '/nte/EDU'
    else:
        t[0] = t[1].split('/')[0] + '/nte/EDU'
    ner_result.append(t[0])
    # educ.writelines(t[0].encode('utf-8') + '\n')

def p_lab(t):
    '''
    lab : LAB
        | ADDRESS ADDRESS NOUN ADJECTIVE NOUN LAB_POST
        | ADDRESS NOUN NOUN LAB_POST
        | ADDRESS NOUN LAB_POST
        | NOUN LAB_POST
        | ADDRESS ADJECTIVE NOUN LAB_POST
        | ORG NOUN LAB_POST
        | NOUN NOUN LAB_POST
        | ADDRESS ADDRESS NOUN NOUN NOUN LAB_POST
        | ADDRESS ADDRESS NOUN LAB_POST
        | ADDRESS NOUN PARTICLE NOUN NOUN NOUN LAB_POST
        | MEASURE NOUN LAB_POST
        | ADDRESS NOUN MAJOR NOUN LAB_POST
        | PERSON LAB_POST
        | ADDRESS MEASURE NOUN LAB_POST
        | ORG ADDRESS NOUN_OTHER LAB_POST
        | MAJOR LAB_POST
        | NOUN STR LAB_POST
    '''
    if len(t) == 8:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + t[5].split('/')[0] + t[6].split('/')[0] + t[7].split('/')[0] + '/ntr/LAB'
    elif len(t) == 7:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + t[5].split('/')[0] + t[6].split('/')[0] + '/ntr/LAB'
    elif len(t) == 6:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + t[5].split('/')[0] + '/ntr/LAB'
    elif len(t) == 5:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + '/ntr/LAB'
    elif len(t) == 4:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + '/ntr/LAB'
    elif len(t) == 3:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + '/ntr/LAB'
    else:
        t[0] = t[1].split('/')[0] + '/ntr/LAB'
    ner_result.append(t[0])
    # lab.writelines(t[0].encode('utf-8') + '\n')
    

def p_company(t):
    '''
    company : COM
            | NOUN_OTHER NOUN COM_POST
            | ADDRESS NOUN COM_POST
            | ADDRESS ADDRESS NOUN COM_POST
            | ADDRESS NOUN NOUN_OTHER NOUN_OTHER COM_POST
            | ADDRESS NOUN NOUN_OTHER NOUN NOUN COM_POST
            | ADDRESS NOUN NOUN COM_POST
            | ADDRESS ADDRESS NOUN NOUN COM_POST
            | ADDRESS PERSON COM_POST
            | ADDRESS NOUN COM
            | PERSON NOUN NOUN COM_POST
            | ADDRESS ADJECTIVE ADJECTIVE NOUN COM_POST
            | TERM COM_POST
            | TERM NOUN COM_POST
            | MEASURE NOUN COM_POST
            | ADDRESS MEASURE ADJECTIVE COM_POST
            | ADDRESS TERM NOUN COM_POST
            | ADDRESS TERM NOUN NOUN COM_POST
            | TERM ADDRESS COM_POST
            | ADDRESS ADDRESS NOUN NOUN NOUN COM_POST
    '''
    if len(t) == 7:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + t[5].split('/')[0] + t[6].split('/')[0] + '/ntc/COM'
    elif len(t) == 6:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + t[5].split('/')[0] + '/ntc/COM'
    elif len(t) == 5:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + t[4].split('/')[0] + '/ntc/COM'
    elif len(t) == 4:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + t[3].split('/')[0] + '/ntc/COM'
    elif len(t) == 3:
        t[0] = t[1].split('/')[0] + t[2].split('/')[0] + '/ntc/COM'
    else:
        t[0] = t[1].split('/')[0] + '/ntc/COM'
    ner_result.append(t[0])
    # company.writelines(t[0].encode('utf-8') + '\n')
    

def p_government(t):
    '''
    government : GOV
    '''
    t[0] = t[1] + '/GOV'
    ner_result.append(t[0])
    

def p_organization(t):
    '''
    organization : ORG
    '''
    t[0] = t[1] + '/ORG'
    ner_result.append(t[0])
    

def p_date(t):
    '''date : DATE
            | DATE DATE
            | DATE DATE DATE
            | DATE DATE DATE DATE
            | DATE DATE DATE DATE DATE
            | DATE DATE DATE DATE DATE DATE
            | DATE_OTHER DATE_OTHER
            | MEASURE MEASURE MEASURE
    '''
    if len(t) == 7:
        t[0] = t[1] + '::' + t[2] + '::' + t[3] + '::' + t[4] + '::' + t[5] + '::' + t[6] + '/DAT'
    elif len(t) == 6:
        t[0] = t[1] + '::' + t[2] + '::' + t[3] + '::' + t[4] + '::' + t[5] + '/DAT'
    elif len(t) == 5:
        t[0] = t[1] + '::' + t[2] + '::' + t[3] + '::' + t[4] + '/DAT'
    elif len(t) == 4:
        t[0] = t[1] + '::' + t[2] + '::' + t[3] + '/DAT'
    elif len(t) == 3:
        t[0] = t[1] + '::' + t[2] + '/DAT'
    else:
        t[0] = t[1] + '/DAT'

    ner_result.append(date_normalizer.normalizer(t[0]))
    # date.writelines(t[0].encode('utf-8') + '\n')

def p_error(t):
    global pre_token
    pre_token = ner_parser.symstack[-1].value
    # if t != None:
    #     print "Syntax error at %s type %s" % (t.value, t.type)
    # if t != None:
      # ner_result.append(t.value + ' O')

def start_parsing(data):
    ner_parser.parse(data, lexer=ner_lexer)

# educ = open('/workspace/svn/search/ie/parser/sample_output/educ.txt', 'a')
# company = open('/workspace/svn/search/ie/parser/sample_output/company.txt', 'a')
# date = open('/workspace/svn/search/ie/parser/sample_output/date.txt', 'a')
# lab = open('/workspace/svn/search/ie/parser/sample_output/lab.txt', 'a')

ner_parser = yacc.yacc(outputdir="./parser/ner")

log = logging.getLogger()