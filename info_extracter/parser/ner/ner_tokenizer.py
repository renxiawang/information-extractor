#!/usr/bin/python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------
# ner_tokenizer.py
#
# 
# 
# ------------------------------------------------------------
import ply.lex as lex

# List of token names.   This is always required
tokens = (
    'ADDRESS',
    'PERSON',
    'POSITION',
    'POS_PRE', 
    'MAJOR',
    'INDUSTRY',
    'ORG',
    'EDU',
    'LAB',
    'COM',
    'GOV',
    'DATE',
    'RACE',
    'SEX',
    'HONOR',
    'TITLE',
    'DEGREE',
    'ADD_POST',
    'EDU_POST',
    'LAB_POST',
    'COM_POST',
    'MAJOR_POST',
    'HONOR_POST',
    'NOUN',
    'TERM',
    'NOUN_OTHER',
    'DATE_OTHER',
    'SPACE',
    'DIRECTION',
    'VERB',
    'ADJECTIVE',
    'DISTINGUISH',
    'STATUS',
    'PRONOUN',
    'MEASURE',
    'QUANTIFIER',
    'ADVERB',
    'PREPOSITION',
    'CONJUNCTION',
    'PARTICLE',
    'INTERJ',
    'TONE',
    'ONOMATOPOEIA',
    'PREFIX',
    'POSTFIX',
    'STR',
    'PUNCTUATION', 
    'OTHER',
    'NEWLINE'
)

# Regular expression rules for simple tokens
t_EDU           =  u"[a-zA-Z]*[\u4e00-\u9fa5]+/(nte)"
t_LAB           =  u"[\u4e00-\u9fa5]+/(ntr)"
t_COM           =  u"[\u4e00-\u9fa5]+/(ntc)"
t_GOV           =  u"[\u4e00-\u9fa5]+/(ntg)"
t_TITLE         =  u"[\u4e00-\u9fa5]+/(nti)"
t_DEGREE        =  u"[\u4e00-\u9fa5]+/(nde)"
t_ADDRESS       =  u"[\u4e00-\u9fa5]+/ns[f]?"
t_PERSON        =  u"[\u4e00-\u9fa5]+/(nr[jf12]?)"
t_POSITION      =  u"[\u4e00-\u9fa5]+/(np)"
t_MAJOR         =  u"[\u4e00-\u9fa5]+/(nm)"
t_INDUSTRY      =  u"[\u4e00-\u9fa5]+/(ni)"
t_ORG           =  u"[\u4e00-\u9fa5]+/(nt)"
t_HONOR         =  u"[\u4e00-\u9fa5]+/(nh)"
t_NOUN          =  u"[\u4e00-\u9fa5]+/(n)"
t_TERM          =  u"[\u4e00-\u9fa5]+/(nz)"
t_NOUN_OTHER    =  u"[\u4e00-\u9fa5]+/(nl|ng)"
t_DATE_OTHER    =  u"[\u4e00-\u9fa5]+/(tg)|[0-9]{4}-/m|[0-9]{4}\.[0-9]{2}-/m"
t_SPACE         =  u"[\u4e00-\u9fa5]+/(s)"
t_DIRECTION     =  u"[\u4e00-\u9fa5]+/(f)"
t_VERB          =  u"[\u4e00-\u9fa5]+/(v[a-z0-9]*)"
t_ADJECTIVE     =  u"[\u4e00-\u9fa5]+/(a[a-z0-9]*)"
t_DISTINGUISH   =  u"[\u4e00-\u9fa5]+/(b[a-z0-9]*)"
t_STATUS        =  u"[\u4e00-\u9fa5]+/(z)"
t_PRONOUN       =  u"[\u4e00-\u9fa5]+/(r[a-z0-9]*)"
t_MEASURE       =  u"(.)+/(m[a-z]*)"
t_QUANTIFIER    =  u"[\u4e00-\u9fa5]+/(q[a-z0-9]*)"
t_ADVERB        =  u"[\u4e00-\u9fa5]+/(d[a-z0-9]*)"
t_PREPOSITION   =  u"[\u4e00-\u9fa5]+/(p[a-z0-9]*)"
t_CONJUNCTION   =  u"[\u4e00-\u9fa5]+/(c+)"
t_PARTICLE      =  u"[\u4e00-\u9fa5]+/(u[a-z0-9]*)"
t_INTERJ        =  u"[\u4e00-\u9fa5]+/(e)"
t_TONE          =  u"[\u4e00-\u9fa5]+/(y)"
t_ONOMATOPOEIA  =  u"[\u4e00-\u9fa5]+/(o)"
t_PREFIX        =  u"[\u4e00-\u9fa5]+/(h)"
t_POSTFIX       =  u"[\u4e00-\u9fa5]+/(k)"
t_STR           =  u"(.)+/(x[a-z]*)"
t_PUNCTUATION   =  u"(.)+/(w[a-z]*)"

t_EDU_POST      =  u"(大学|学院|中学|小学|高中|学校|高级中学)+/(n|nl)"
t_LAB_POST      =  u"(研究院|研究所|科学院|实验室|科学院)+/(n)"
t_COM_POST      =  u"(公司|集团|有限公司|银行)+/(n)"
t_MAJOR_POST    =  u"(专业|系)+/(n|v)"
t_HONOR_POST    =  u"(奖|典范)+/(n)"

t_OTHER         =  u".+"

def t_DATE(t):
    # 1963年 t, 12月 t, 一九四二年 t, 十一月 t
    # 2007 m(\n)01 m(\n)18 m(\n)
    # 15301603年 t
    # 1974- m(\n)今 tg
    # 1929-1939 m
    # 1998.062003.04 m
    # 2004.02- m(\n)今 tg
    # 1986-87, 95-96 m
    # 1981.4.24 m, 1996.6 m 
    u"""[零〇一二三四五六七八九十0-9]{1,4}[年月日时分秒]+/t|
    [0-9]{4}/m(\n)[0-9]{2}/m(\n)[0-9]{2}/m|
    [0-9]{8}年+/t|
    [0-9]{4}-[0-9]{4}/m|
    [0-9]{4}\.[0-9]{6}\.[0-9]{2}/m|
    [0-9]{2,4}-[0-9]{2}/m|
    [0-9]{4}(\.[0-9]{1,2}){1,2}/m|
    [0-9]{4}\.[0-9]{1}-[0-9]{4}\.[0-9]{1}/m"""
    return t

def t_RACE(t):
    u"[\u4e00-\u9fa5]+/(nra)"
    return t

def t_SEX(t):
    u"[\u4e00-\u9fa5]+/(nse)"
    return t

def t_POS_PRE(t):
    u"(副|原|资深)/b"
    return t

def t_ADD_POST(t):
    u"(国|省|市|区|镇|乡)+/(n)"
    return t

# Define a rule so we can track line numbers
def t_NEWLINE(t):
    r'\n\n'
    t.lexer.lineno += len(t.value)
    return t

# A string containing ignored characters (spaces and tabs)
t_ignore  = ' \t'

# Error handling rule
def t_error(t):
    # print "Illegal character '%s'" % (t.value[0])
    t.lexer.skip(1)

# Build the lexer
ner_lexer = lex.lex()

# Test it out
data = '''个人/n
简介/n
:/wp
马欣川/nr
，/wd
男/nse
。/wj
资深/b
职业/n
顾问/np
、/wn
心理学/nm
博士/nde
、/wn
人才测评/nm
博士后/nde
、/wn
原/b
华南师范大学/nte
人力资源研究所/ntr
副/b
所长/np
、/wn
国内/s
最/d
具/vg
实战/n
经验/n
的/ude1
人才测评/nm
专家/n
之一/rz
。/wj
现/tg
任职/v
于/p
深圳大学/nte
。/wj'''.decode('utf-8')
ner_lexer.input(data)
while True:
    tok = ner_lexer.token()
    if not tok: break      # No more input
    print "'" + tok.value.encode('utf-8') + "'" + ', ' + tok.type.encode('utf-8')
