#!/usr/bin/python
# -*- coding: utf-8 -*-

import ConfigParser
import json
import pymongo
import bson
import nlpir
import os
import paras
import datetime
import cosine_similarity as csimi
import parser.ner.ner_parser as ner_parser
import parser.err.err_extractor as err_extractor 

from pymongo import Connection
from bson.objectid import ObjectId
from bson.son import SON

def init():
    """
        Function: Load configuration of mongodb, NER and EER
    """
    # init configures
    config = ConfigParser.ConfigParser()
    with open(paras.config_path, 'r') as ie_cfg:
        config.readfp(ie_cfg)

        # load mongodb config
        paras.db_ip = config.get('mongodb', 'db_ip')
        paras.db_port = config.getint('mongodb', 'db_port')
        paras.db_usr = config.get('mongodb', 'db_usr')
        paras.db_pwd = config.get('mongodb', 'db_pwd')

        # load ictclas config
        paras.ict_path = config.get('ictclas', 'ict_path')
        paras.ict_dict = config.get('ictclas', 'ict_dict')

        # load NER and ERR path
        paras.ner_path = config.get('NER', 'ner_path')
        paras.ner_file = config.get('NER', 'ner_file')
        paras.err_path = config.get('ERR', 'err_path')
        paras.err_file = config.get('ERR', 'err_file')
        paras.err_result = config.get('ERR', 'err_result')
    
    # init ictclas and load user dict
    print nlpir.ict_init(paras.ict_path)
    # print ictclas.import_dict(paras.ict_dict)

    # init mongodb connection
    paras.db_conn = Connection(paras.db_ip, paras.db_port)
    paras.db = paras.db_conn['renwu_fyp']

def process_data():
    """
        Function: Extract useful data from original mongodb database
        Return: A dict contains a person's information
    """
    collection = paras.db['hudong']
    people = collection.find()
    # text_file = open('./parser/sample_output/text_file.txt', 'w')

    for person in people:    
        # encode to utf-8
        encoded_person = encode_person(person)
        node_description = []
        node_attributes = {}
        
        # multi-value nodes
        node_usedNames = []
        node_titles = []
        node_photos = []
        # node_careers = []
        node_hobbies = []
        node_blogs = []
        node_microblogs = []
        node_qqs = []
        node_msns = []
        node_emails = []
        node_educations = []
        node_workExperience = []
        node_opuses = []
        node_awards = []
        node_relationNet = []
        
        # 简介
        if paras.text_keys[0] in encoded_person.keys():
            temp_dict = {}
            temp_dict['label'] = paras.text_keys[0]
            temp_dict['content'] = encoded_person.get(paras.text_keys[0])
            node_description.append(temp_dict)

        # 个人简介, 教育经历, 工作经历, 工作成就及荣誉
        for i in range(1, 4):
            labels = [s for s in encoded_person.keys() if paras.text_keys[i] in s]
            for x in labels:
                temp_dict = {}
                temp_dict['label'] = x
                temp_dict['content'] = encoded_person.get(x)
                node_description.append(temp_dict)
        
        # 曾用名，头衔， 荣誉...
        for i in range(0 ,len(paras.attributes )- 1):
            if paras.attributes.keys()[i] in encoded_person.keys():
                if paras.attributes.values()[i] == 'usedName':
                    temp_dict = {}
                    temp_dict['name'] = encoded_person.get(paras.attributes.keys()[i])
                    node_usedNames.append(temp_dict)
                elif paras.attributes.values()[i] == 'organization':
                    temp_dict = {}
                    temp_dict['organization'] = encoded_person.get(paras.attributes.keys()[i])
                    node_workExperience.append(temp_dict)
                elif paras.attributes.values()[i] == 'position':
                    temp_dict = {}
                    temp_dict['position'] = encoded_person.get(paras.attributes.keys()[i])
                    node_workExperience.append(temp_dict)
                elif paras.attributes.values()[i] == 'hobby':
                    temp_dict = {}
                    temp_dict['hobby'] = encoded_person.get(paras.attributes.keys()[i])
                    node_hobbies.append(temp_dict)
                elif paras.attributes.values()[i] == 'school':
                    temp_dict = {}
                    temp_dict['school'] = encoded_person.get(paras.attributes.keys()[i])
                    node_educations.append(temp_dict)
                elif paras.attributes.values()[i] == 'degree':
                    temp_dict = {}
                    temp_dict['degree'] = encoded_person.get(paras.attributes.keys()[i])
                    node_educations.append(temp_dict)
                elif paras.attributes.values()[i] == 'opus':
                    temp_dict = {}
                    temp_dict['opus'] = encoded_person.get(paras.attributes.keys()[i])
                    node_opuses.append(temp_dict)
                elif paras.attributes.values()[i] == 'award':
                    temp_dict = {}
                    temp_dict['award'] = encoded_person.get(paras.attributes.keys()[i])
                    node_awards.append(temp_dict)
                elif paras.attributes.values()[i] == 'relationNet':
                    temp_dict = {}
                    temp_dict['name'] = encoded_person.get(paras.attributes.keys()[i])
                    temp_dict['relationship'] = paras.attributes.keys()[i]
                    node_relationNet.append(temp_dict)
                elif paras.attributes.values()[i] == 'title':
                    temp_dict = {}
                    temp_dict['title'] = encoded_person.get(paras.attributes.keys()[i])
                    node_titles.append(temp_dict)
                else:
                    node_attributes[paras.attributes.values()[i]] = encoded_person.get(paras.attributes.keys()[i])
                    paras.existed_simple_attrs[paras.attributes.values()[i]] = 1

        # combine all nodes
        # TODO if node_description is empty
        if len(node_description) > 0:
            node_attributes['description'] = node_description
        if len(node_usedNames) > 0:
            node_attributes['usedNames'] = node_usedNames
            paras.existed_advanced_attrs['usedNames'] = 1
        if len(node_workExperience) > 0:
            node_attributes['workExperience'] = node_workExperience
            paras.existed_advanced_attrs['workExperience'] = 1
        if len(node_hobbies) > 0:
            node_attributes['hobbies'] = node_hobbies
            paras.existed_advanced_attrs['hobbies'] = 1
        if len(node_educations) > 0:
            node_attributes['educations'] = node_educations
            paras.existed_advanced_attrs['educations'] = 1
        if len(node_opuses) > 0:
            node_attributes['opuses'] = node_opuses
            paras.existed_advanced_attrs['opuses'] = 1
        if len(node_awards) > 0:
            node_attributes['awards'] = node_awards
            paras.existed_advanced_attrs['awards'] = 1
        if len(node_relationNet) > 0:
            node_attributes['relationNet'] = node_relationNet
            paras.existed_advanced_attrs['relationNet'] = 1
        if len(node_titles) > 0:
            node_attributes['titles'] = node_titles
            paras.existed_advanced_attrs['titles'] = 1

        node_attributes['timestamp'] = datetime.datetime.now()
        
        '''
        check processed outcome
        '''
        # db_connection = Connection(paras.db_ip, paras.db_port)
        # db_test = db_connection.test_only
        # collec_people = db_test.people
        # collec_people.insert(node_attributes)

        # word segmentation by ICTCLAS
        segmented_text = segment_word(node_description)
        # test seg output
        # print segmented_text
        # break

        if segmented_text != False:
            # name entity recognition
            # return a segmented and tagged sentences
            result_list = ner(segmented_text)
            result_str = ''
            temp_str = ''
            # segmented by sentences

            for x in result_list:
                result_str = result_str + x + ' '
                if x.encode('utf-8') == '。/wj/O':
                    result_str += '\n\n'

            for line in result_str.encode('utf-8').split('\n\n'):
                edu, work, extracted_simple_attrs = err_extractor.entity_relation(line, paras.existed_simple_attrs, paras.existed_advanced_attrs)
                node_attributes = update_person(node_attributes, edu, work, extracted_simple_attrs)

        # write result to mongodb
        insert_to_mongodb(node_attributes)
        paras.reset()

def encode_person(person):
    """
        Function: Encode dict person to utf-8
        Return: A encoded dict person
    """
    encoded_person = {}
    for (k, v) in person.items():
        if (v == None or v == "" or v == '无任何介绍'.decode('utf-8') or k == '_id'):
            continue

        encoded_person[k.encode('utf-8')] = v.encode('utf-8')
    return encoded_person

def segment_word(description):
    """
        Function: Segmenting words by ICTCLAS
    """
    # prepare text
    input_text = ''
    for x in description:
        input_text = input_text + x['label'] + ':' + x['content'] + '\n'
    
    # test
    # print input_text

    if input_text == '':
        return False
    else:
        # convert to GB2312 to satisfy the ICTCLAS's requirement
        encoded_text = input_text.replace(' ', '').replace('　　', '').decode('utf-8').encode('gb2312', 'ignore')

        # segmentation
        tokens = nlpir.process_str(encoded_text, 1)

        # return result
        return tokens.replace('\n', '\n\n').replace(' ', '\n').decode('gb2312').encode('utf-8')

def ner(segmented_text):
    """
        Function: Name Entity Recognition
    """
    ner_parser.start_parsing(segmented_text.decode('utf-8'))
    ner_result = ner_parser.ner_result
    ner_parser.ner_result = []
    return ner_result
    
def err(ner_result):
    """
        Function: Entity Relationship Recognition
    """

def update_person(person, edu, work, extracted_simple_attrs):
    """
        Function: Update person's information according to err()'s output
    """
    # update simple attributes
    if len(extracted_simple_attrs) > 0:
        for (k, v) in extracted_simple_attrs.items():
            person[k] = v
            paras.existed_simple_attrs[k] = 1
    
    # update educations
    if len(edu) > 0:
        # TODO compare edu with existing one
        if person.has_key("educations") == False:
            # new_edu = []
            # person["educations"] = new_edu
            person["educations"] = edu
        # person["educations"].append(edu)
        else:
            person["educations"].extend(edu)
    
    # update work
    if len(work) > 0:
        # TODO compare work with existing one
        if person.has_key("workExperience") == False:
            # new_work = []
            # person["workExperience"] = new_work
            person["workExperience"] = work
        # person["workExperience"].append(work)
        else:
            person["workExperience"].extend(work)

    return person


def insert_to_mongodb(person):
    collection = paras.db['hudong_extracted']
    collection.insert(person)
    pass

def main():
    init()
    process_data()
    nlpir.ict_exit()
    
if __name__ == '__main__':
    main()
