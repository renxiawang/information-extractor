#!/usr/bin/python
# -*- coding: utf-8 -*-
# global variales

# config file path 
config_path = './ie.cfg'

# mongodb default config
db_ip = None
db_port = None
db_usr = None
db_pwd = None

# ictclas default config
ict_path = None
ict_dict = None

# Mongodb instance
db_conn = None
db = None

# NER parser path
ner_path = None
ner_file = None

# ERR parser path
err_path = None
err_file = None
err_result = None

# keys
text_keys = ['简介', '个人简介', '教育经历', '工作经历', '工作成就及荣誉']
attributes = {
    'personLink':'dataSource', 
    '中文名':'name', 
    '中文全名':'name',
    '姓名':'name',
    '别名':'nickName', 
    '绰号':'nickName',
    '外号':'nickName',
    '艺名':'nickName',
    '昵称':'nickName',
    '原名':'usedName',         # -> period, name
    '曾用名':'usedName',        # -> period, name
    '国籍':'nationality', 
    '国家':'nationality',
    '现居地':'address',
    '现居住地':'address',
    '现住地':'address',
    '现居':'address',
    'IMG':'photo', 
    '性别':'gender', 
    '身高':'height', 
    '身 高':'height',          
    '身高/体重':'height',       # -> height, weight
    '身高体重':'height',        # -> height, weight
    '身高、体重':'height',       # -> height, weight
    '体重':'weight', 
    '体 重':'weight',
    '三围':'BWH', 
    '出生地':'birthPlace', 
    '籍贯':'birthPlace',
    '家乡':'birthPlace',
    '故乡':'birthPlace',
    '出生日期':'birthday', 
    '生日':'birthday',
    '星 座':'constellation',
    '星座':'constellation',
    '星座/血型':'constellation',
    '逝世日期':'deadtime',
    '民族':'race', 
    '种族':'race',
    '政治面貌':'politicalStatus', 
    '政党':'politicalStatus',
    '所属党派':'politicalStatus',
    '所属公司':'organization',  # -> industry, org, pos, desc
    '任职公司':'organization',  # -> industry, org, pos, desc
    '职业':'position',         # -> industry, org, pos, desc
    '任职':'position',         # -> industry, org, pos, desc
    '现任职务':'position',      # -> industry, org, pos, desc
    '官职':'position',         # -> industry, org, pos, desc
    '官位':'position',         # -> industry, org, pos, desc
    '历任职务':'position',      # -> industry, org, pos, desc
    '单位':'organization',     # -> industry, org, pos, desc
    '签约公司':'organization',  # -> industry, org, pos, desc
    '爱好':'hobby',            # -> hobbies
    '兴趣':'hobby',            # -> hobbies
    '个人爱好':'hobby',         # -> hobbies
    '博客':'blog', 
    '微博':'microblog', 
    '新浪微博':'microblog',
    'QQ':'qq', 
    'MSN':'msn', 
    '邮箱':'email', 
    '毕业院校':'school',        # -> educations
    '学历':'school',           # -> educations
    '学位':'degree',           # -> educations
    '教育背景':'school',        # -> educations
    '毕业院校':'school',        # -> educations
    '专业':'major', 
    '职位':'position', 
    '作品':'opus',             # -> opuses
    '代表作品':'opus',          # -> opuses
    '作品名称':'opus',          # -> opuses
    '主要作品':'opus',          # -> opuses
    '个人专辑':'opus',
    '荣誉':'award',            # -> awards
    '主要奖项':'award',         # -> awards
    '主要荣誉':'award',         # -> awards
    '获得荣誉':'award',         # -> awards
    '所获荣誉':'award',         # -> awards
    '荣誉称号':'award',         # -> awards
    '获奖':'award',            # -> awards
    '配偶':'relationNet',      # -> name, type, relationship
    '妻子':'relationNet',      # -> name, type, relationship
    '父亲':'relationNet',      # -> name, type, relationship
    '丈夫':'relationNet',      # -> name, type, relationship
    '母亲':'relationNet',      # -> name, type, relationship
    '儿子':'relationNet',      # -> name, type, relationship
    '女儿':'relationNet',      # -> name, type, relationship
    '教练':'relationNet',      # -> name, type, relationship
    '家庭成员':'relationNet',   # -> name, type, relationship
    '子女':'relationNet',      # -> name, type, relationship
    '师傅':'relationNet',      # -> name, type, relationship
    '家人':'relationNet',      # -> name, type, relationship
    '哥哥':'relationNet',      # -> name, type, relationship
    '经纪人':'relationNet',    # -> name, type, relationship
    '好友':'relationNet',      # -> name, type, relationship
    '徒弟':'relationNet',      # -> name, type, relationship
    '弟弟':'relationNet',      # -> name, type, relationship
    '姐姐':'relationNet',      # -> name, type, relationship
    '师父':'relationNet',      # -> name, type, relationship
    '搭档':'relationNet',      # -> name, type, relationship
    '师从':'relationNet',      # -> name, type, relationship
    '师弟':'relationNet',      # -> name, type, relationship
    '夫人':'relationNet',      # -> name, type, relationship
    '女友':'relationNet',      # -> name, type, relationship
    '爱人':'relationNet',      # -> name, type, relationship
    '前妻':'relationNet',      # -> name, type, relationship
    '父母':'relationNet',      # -> name, type, relationship
    '妹妹':'relationNet',      # -> name, type, relationship
    '亲人':'relationNet',      # -> name, type, relationship
    '相关人物':'relationNet',   # -> name, type, relationship
    '圈中好友':'relationNet',   # -> name, type, relationship
    '祖父':'relationNet',      # -> name, type, relationship
    '指导教师':'relationNet',   # -> name, type, relationship
    '恋人':'relationNet',      # -> name, type, relationship
    '学生':'relationNet',      # -> name, type, relationship
    '朋友':'relationNet',      # -> name, type, relationship
    '心上人':'relationNet',     # -> name, type, relationship
    '师兄':'relationNet',      # -> name, type, relationship
    '前夫':'relationNet',      # -> name, type, relationship
    '男友':'relationNet',
    '职称':'title',            # -> titles
    '军衔':'title',            # -> titles
    '头衔':'title'
}

# existed attrs
existed_simple_attrs = {
    "nickName" : 0,
    "nationality" : 0,
    "address": 0,
    "zipCode": 0,
    "gender": 0,
    "height": 0,
    "weight": 0,
    "BWH": 0,
    "birthPlace": 0,
    "birthday": 0,
    "constellation": 0,
    "deadtime": 0,
    "race": 0,
    "politicalStatus": 0,
    "email": 0
}

existed_advanced_attrs = {
    "usedNames" : 0,
    "titles" : 0,
    "photos" : 0,
    "careers" : 0,
    "hobbies" : 0,
    "blogs" : 0,
    "microblogs" : 0,
    "emails" : 0,
    "educations" : 0,
    "workExperience" : 0,
    "opuses" : 0,
    "awards" : 0,
    "relationNet" : 0
}

def reset():
    global existed_simple_attrs
    global existed_advanced_attrs
    existed_simple_attrs = {
        "nickName" : 0,
        "nationality" : 0,
        "address": 0,
        "zipCode": 0,
        "gender": 0,
        "height": 0,
        "weight": 0,
        "BWH": 0,
        "birthPlace": 0,
        "birthday": 0,
        "constellation": 0,
        "deadtime": 0,
        "race": 0,
        "politicalStatus": 0,
        "email": 0
    }
    existed_advanced_attrs = {
        "usedNames" : 0,
        "titles" : 0,
        "photos" : 0,
        "careers" : 0,
        "hobbies" : 0,
        "blogs" : 0,
        "microblogs" : 0,
        "emails" : 0,
        "educations" : 0,
        "workExperience" : 0,
        "opuses" : 0,
        "awards" : 0,
        "relationNet" : 0
    }

