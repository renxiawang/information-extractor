#!/usr/bin/python
# -*- coding: utf-8 -*-

import nltk
from collections import defaultdict

def get_vectors(str1, str2):
    """
        Function: Generate vectors for 2 string
        Return: 2 vectors
    """
    # TODO add str encoding checking
    char_in_str1 = set(str1.decode('utf-8'))
    char_in_str2 = set(str2.decode('utf-8'))
    all_char = char_in_str1 | char_in_str2

    # vectors
    vector_1 = defaultdict(int)
    vector_2 = defaultdict(int)

    # if character existed in str, 
    # then set corresponding value to 1 in vector
    # if not, set to 0
    for e in all_char:
        vector_1[e] = str1.decode('utf-8').count(e)
        vector_2[e] = str2.decode('utf-8').count(e)

    print vector_1.values()
    print vector_2.values()

    return vector_1.values(), vector_2.values()

def get_similarity(str1, str2):
    """
        Function: Compute similarity of two strings
        Return: Cosine distance between two strings
        Tool: nltk
    """
    v1, v2 = get_vectors(str1, str2)
    return nltk.cluster.util.cosine_distance(v1, v2)
