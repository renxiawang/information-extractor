Steps for compiling:

    1. sudo apt-get install libboost-python-dev
    2. (32bit)sudo g++ ictclas.cpp -shared -o ictclas.so -I/usr/include/python2.7 -lboost_python -L. -lICTCLAS2011 -m32 -DOS_LINUX
       (64bit)sudo g++ ictclas.cpp -shared -o ictclas.so -I/usr/include/python2.7 -lboost_python -L. -lICTCLAS2011 -m64 -DOS_LINUX -fPIC
    3. sudo cp ictclas.so /usr/lib/python2.7/ictclas.so

    4. export LD_LIBRARY_PATH=/workspace/ICTCLAS2012
Sample code(Python):

    import ictclas
    
    print ictclas.ict_init("./") # path to ICTCLAS2012
    t_filename = "/tmp/sample_bad.txt" # sample text for segmentation
    tweet_file = open(t_filename, 'r')
    for line in tweet_file.readlines():
        string2 = line.decode('utf-8').encode('gb2312', 'ignore')
        tokens = ictclas.process_str(string2, 0)
        print tokens.decode('gb2312').encode('utf-8')
    print ictclas.ict_exit()
