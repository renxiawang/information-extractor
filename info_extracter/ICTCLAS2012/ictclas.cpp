#include <boost/python.hpp>
#include <boost/python/list.hpp>
#include "ICTCLAS2011.h"

using namespace boost::python;

bool ict_init(const char* pDirPath)
{
    return ICTCLAS_Init(pDirPath);
}

bool ict_exit()
{
    return ICTCLAS_Exit();
}

unsigned int import_dict(const char *sFilename)
{
    return ICTCLAS_ImportUserDict(sFilename);
}

const char * process_str(const char *sParag, int bTagged)
{
    return ICTCLAS_ParagraphProcess(sParag, bTagged);
}

list process_str_ret_list(const char *sParag)
{
    int pResultCount = 0;
    const result_t* re = ICTCLAS_ParagraphProcessA(sParag, &pResultCount);
    list result;
    for (int i=0; i<pResultCount; i++)
    {
        result.append(re[i]);
    }
    return result;
}

bool process_file(const char *sSrcFilename,const char *sRetFilename,int bTagged)
{
    return ICTCLAS_FileProcess(sSrcFilename, sRetFilename, bTagged);
}

BOOST_PYTHON_MODULE(ictclas)
{
    class_<result_t>("result_t")
        .def_readonly("start", &result_t::start)
        .def_readonly("length", &result_t::length)
        .def_readonly("pos_id", &result_t::iPOS)
        .def_readonly("word_id", &result_t::word_ID);
    def("ict_init", ict_init);
    def("ict_exit", ict_exit);
    def("import_dict", import_dict);
    def("process_str", process_str);
    def("process_str_ret_list", process_str_ret_list);
    def("process_file", process_file);
}
